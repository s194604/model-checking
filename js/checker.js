document.getElementById("prop btn").addEventListener("click", checkProp);
document.getElementById("modal btn").addEventListener("click", checkModal);
document.getElementById("ltl btn").addEventListener("click", checkTemporal);

function formatPropOutput(outputArray) {
    if (outputArray[0] == 0 || outputArray[0] == undefined) {
        return "No valid nodes.";
    }
    var outputString = "Valid nodes: ";
    var i = 0;
    while (outputArray[i] != 0 && outputArray[i] != undefined) {
        outputString += outputArray[i];
        if (outputArray[i + 1] == 0 || outputArray[i + 1] == undefined) {
            outputString += ". ";
        } else {
            outputString += ", ";
        }
        i += 1;
    }
    return outputString;
}

function formatModalOutput(outputArray) {
    return formatPropOutput(outputArray);
}

function formatTemporalOutput(outputA, outputB, length) {
    if (length == 0) {
        return "No valid nodes.";
    }
    var outputString = "Valid nodes: ";
    var i = 0;
    var a;
    var b;
    while (i < length) {
        if (outputA[i] == 0) {a = '∞'} else {a = outputA[i]}
        if (outputB[i] == 0) {b = '∞'} else {b = outputB[i]}
        if (a == b) {outputString += a} else {
            outputString += a;
            outputString += "..";
            outputString += b;
        }
        if (i + 1 == length) {
            outputString += ". ";
        } else {
            outputString += ", ";
        }
        i += 1;
    }
    return outputString;
}

async function checkProp() {
    const instance = await WebAssembly.instantiate(await WebAssembly.compileStreaming(fetch("http://localhost:8000/zig-out/lib/checker.wasm")), {
        printer: {
            print(x) {
              console.log(x);
            }
        },
    });

    const w = document.getElementById("prop w").value;
    const r = document.getElementById("prop r").value;
    const v = document.getElementById("prop v").value;
    const e = document.getElementById("prop e").value;

    const arr0 = Array.from(r);
    const arr1 = Array.from(v);
    const arr2 = Array.from(e);

    const input0 = new Uint32Array(instance.exports.memory.buffer, 0, arr0.length + 1);
    for (var i = arr0.length - 1; i >= 0; i--) {
        input0[i] = arr0[i].charCodeAt(0);
    }
    input0[arr0.length] = 0;
    const input1 = new Uint32Array(instance.exports.memory.buffer, input0.byteOffset + (arr0.length + 1) * 4, arr1.length + 1);
    for (var i = arr1.length - 1; i >= 0; i--) {
        input1[i] = arr1[i].charCodeAt(0);
    }
    input1[arr1.length] = 0;
    const input2 = new Uint32Array(instance.exports.memory.buffer, input1.byteOffset + (arr1.length + 1) * 4, arr2.length + 1);
    for (var i = arr2.length - 1; i >= 0; i--) {
        input2[i] = arr2[i].charCodeAt(0);
    }
    input2[arr2.length] = 0;
    var output = new Uint32Array(instance.exports.memory.buffer);
    instance.exports.checkPropositional(w, input0.byteOffset, input1.byteOffset, input2.byteOffset, output);
    output = new Uint32Array(instance.exports.memory.buffer, output.byteOffset, w);
    document.getElementById("prop output").innerHTML = formatPropOutput(output);
}

async function checkModal() {
    const instance = await WebAssembly.instantiate(await WebAssembly.compileStreaming(fetch("http://localhost:8000/zig-out/lib/checker.wasm")), {
        printer: {
            print(x) {
              console.log(x);
            }
        },
    });

    const w = document.getElementById("modal w").value;
    const r = document.getElementById("modal r").value;
    const v = document.getElementById("modal v").value;
    const e = document.getElementById("modal e").value;

    const arr0 = Array.from(r);
    const arr1 = Array.from(v);
    const arr2 = Array.from(e);

    const input0 = new Uint32Array(instance.exports.memory.buffer, 0, arr0.length + 1);
    for (var i = arr0.length - 1; i >= 0; i--) {
        input0[i] = arr0[i].charCodeAt(0);
    }
    input0[arr0.length] = 0;
    const input1 = new Uint32Array(instance.exports.memory.buffer, input0.byteOffset + (arr0.length + 1) * 4, arr1.length + 1);
    for (var i = arr1.length - 1; i >= 0; i--) {
        input1[i] = arr1[i].charCodeAt(0);
    }
    input1[arr1.length] = 0;
    const input2 = new Uint32Array(instance.exports.memory.buffer, input1.byteOffset + (arr1.length + 1) * 4, arr2.length + 1);
    for (var i = arr2.length - 1; i >= 0; i--) {
        input2[i] = arr2[i].charCodeAt(0);
    }
    input2[arr2.length] = 0;
    var output = new Uint32Array(instance.exports.memory.buffer);
    instance.exports.checkModal(w, input0.byteOffset, input1.byteOffset, input2.byteOffset, output);
    output = new Uint32Array(instance.exports.memory.buffer, output.byteOffset, w);
    document.getElementById("modal output").innerHTML = formatModalOutput(output);
}

async function checkTemporal() {
    const instance = await WebAssembly.instantiate(await WebAssembly.compileStreaming(fetch("http://localhost:8000/zig-out/lib/checker.wasm")), {
        printer: {
            print(x) {
              console.log(x);
            }
        },
    });

    const v = document.getElementById("ltl v").value;
    const e = document.getElementById("ltl e").value;

    const arr0 = Array.from(v);
    const arr1 = Array.from(e);

    const input0 = new Uint32Array(instance.exports.memory.buffer, 0, arr0.length + 1);
    for (var i = arr0.length - 1; i >= 0; i--) {
        input0[i] = arr0[i].charCodeAt(0);
    }
    input0[arr0.length] = 0;
    const input1 = new Uint32Array(instance.exports.memory.buffer, input0.byteOffset + (arr0.length + 1) * 4, arr1.length + 1);
    for (var i = arr1.length - 1; i >= 0; i--) {
        input1[i] = arr1[i].charCodeAt(0);
    }
    input1[arr1.length] = 0;
    var output = new Uint32Array(instance.exports.memory.buffer);
    const length = instance.exports.checkTemporal(input0.byteOffset, input1.byteOffset, output);
    output = new Uint32Array(instance.exports.memory.buffer, output.byteOffset, 2);
    const outputA = new Uint32Array(instance.exports.memory.buffer, output[0], length);
    const outputB = new Uint32Array(instance.exports.memory.buffer, output[1], length);
    document.getElementById("ltl output").innerHTML = formatTemporalOutput(outputA, outputB, length);
}
