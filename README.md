# Model Checking Web App

This is a web app built in Svelte for doing model checking on different types of logical models. The project is under development and aims to be the foundation for a potential e-learning platform. 

## Running the app

### Prerequisites

To run this tool, you must have Zig (https://ziglang.org/) version 10.1 or higher and npm (https://www.npmjs.com/) or similar installed. 

If you're using the snap package manager (like on Ubuntu), you can install these tools as follows:

	$ snap install zig

and

	$ snap install node

If you're using Homebrew (like on MacOS), you can install them as follows:

	$ brew install zig

and

	$ brew install node

### Building and running the app

Assuming git is installed, the repository can be cloned from the GitLab repository:

	$ git clone https://gitlab.gbar.dtu.dk/s194604/model-checking.git


From the root folder of the project, build the WebAssembly:

	$ zig build

This will create a .wasm file located at /svelte/static. To build app, first cd into the svelte directory:

	$ cd svelte

Then, install the dependencies with npm:

	$ npm install

Build the svelte project:

	$ npm run build

Finally, you can run the project in your preferred browser by using the link provided by running:

	$ npm run preview

## Status of this project

This project does not live up to conventional standards as a result of many factors. Hopefully the code will be cleaned up in some future endeavor of mine to make the project potentially useful to others. 

## Known bugs

- A rushed temporary vis.js implementation, which is currently only implemented for propositional logic, causes multi-character propositions to not work for that specific logical system. The implementation also causes the tests to be unable to run. This implementation can effectively be removed by commenting line 23 in src/checker.zig. 
