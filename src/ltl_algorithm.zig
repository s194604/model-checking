const std = @import("std");
const typeData = @import("types.zig");
const Vector = std.meta.Vector;
const ArrayList = std.ArrayList;
const HashMap = std.AutoArrayHashMap;
const allocator = std.heap.page_allocator;
const charType = @import("constants.zig").charType;
const infinity = @import("constants.zig").infinity;
const conctenate = @import("formatter.zig").concatenateLinearValuation;
const appendValuation = @import("formatter.zig").appendValuation;
const stateType = u32;

pub fn checkModel(valuations: *const HashMap(charType, ArrayList(typeData.linearValuation)), expression: *const typeData.Node) []typeData.linearValuation {
    var contextValuations = ArrayList(typeData.linearValuation).init(allocator);
    return check(valuations, expression, 1, &contextValuations);
}

fn check(valuations: *const HashMap(charType, ArrayList(typeData.linearValuation)), expression: *const typeData.Node, state: stateType, contextValuations: *ArrayList(typeData.linearValuation)) []typeData.linearValuation {
    defer contextValuations.clearAndFree();
    var operator: typeData.Type = undefined;
    switch (expression.*) {
        .boolean => |b| {
            if (b.value) {
                contextValuations.append(typeData.linearValuation{
                    .from = state,
                    .until = infinity,
                }) catch unreachable;
            }
            var copy = allocator.alloc(typeData.linearValuation, contextValuations.items.len) catch unreachable;
            std.mem.copy(typeData.linearValuation, copy, contextValuations.items);
            return copy;
        },
        .proposition => |p| {
            if (valuations.*.get(p.value) == null) {
                return &[0]typeData.linearValuation{};
            }
            var copy = allocator.alloc(typeData.linearValuation, valuations.*.get(p.value).?.items.len) catch unreachable;
            std.mem.copy(typeData.linearValuation, copy, valuations.*.get(p.value).?.items);
            return copy;
        },
        .unary => |u| {
            operator = u.type;
        },
        .binary => |b| {
            operator = b.type;
        },
    }
    switch (operator) {
        typeData.Type.TEMPORAL_NEXT => {
            const childValuations = check(valuations, expression.*.unary.child, state + 1, contextValuations);
            defer allocator.free(childValuations);
            return checkNext(childValuations, contextValuations);
        },
        typeData.Type.TEMPORAL_FUTURE => {
            const childValuations = check(valuations, expression.*.unary.child, state, contextValuations);
            defer allocator.free(childValuations);
            return checkFurure(childValuations, contextValuations);
        },
        typeData.Type.TEMPORAL_GLOBAL => {
            const childValuations = check(valuations, expression.*.unary.child, state, contextValuations);
            defer allocator.free(childValuations);
            return checkGlobal(childValuations, contextValuations);
        },
        typeData.Type.TEMPORAL_UNTIL => {
            const fromValuations = check(valuations, expression.*.binary.left, state, contextValuations);
            const untilValuations = check(valuations, expression.*.binary.right, state, contextValuations);
            defer allocator.free(fromValuations);
            defer allocator.free(untilValuations);
            return checkUntil(fromValuations, untilValuations, contextValuations);
        },
        typeData.Type.TEMPORAL_RELEASE => {
            const releaseValuations = check(valuations, expression.*.binary.left, state, contextValuations);
            const pendingValuations = check(valuations, expression.*.binary.right, state, contextValuations);
            defer allocator.free(releaseValuations);
            defer allocator.free(pendingValuations);
            return checkRelease(pendingValuations, releaseValuations, contextValuations);
        },
        typeData.Type.TEMPORAL_WEAKUNTIL => {
            const fromValuations = check(valuations, expression.*.binary.left, state, contextValuations);
            const untilValuations = check(valuations, expression.*.binary.right, state, contextValuations);
            defer allocator.free(fromValuations);
            defer allocator.free(untilValuations);
            return checkWeakUntil(fromValuations, untilValuations, contextValuations);
        },
        typeData.Type.NOT => {
            const childValuations = check(valuations, expression.*.unary.child, state, contextValuations);
            defer allocator.free(childValuations);
            return checkNot(childValuations, contextValuations);
        },
        typeData.Type.AND => {
            const left = check(valuations, expression.*.binary.left, state, contextValuations);
            const right = check(valuations, expression.*.binary.right, state, contextValuations);
            defer allocator.free(left);
            defer allocator.free(right);
            return checkAnd(left, right, contextValuations);
        },
        typeData.Type.OR => {
            const left = check(valuations, expression.*.binary.left, state, contextValuations);
            const right = check(valuations, expression.*.binary.right, state, contextValuations);
            defer allocator.free(left);
            defer allocator.free(right);
            return checkOr(left, right, contextValuations);
        },
        typeData.Type.IF => {
            const left = check(valuations, expression.*.binary.left, state, contextValuations);
            const right = check(valuations, expression.*.binary.right, state, contextValuations);
            defer allocator.free(left);
            defer allocator.free(right);
            const step0 = checkNot(left, contextValuations);
            defer allocator.free(step0);
            contextValuations.clearAndFree();
            return checkOr(step0, right, contextValuations);
        },
        typeData.Type.IFF => {
            const left = check(valuations, expression.*.binary.left, state, contextValuations);
            const right = check(valuations, expression.*.binary.right, state, contextValuations);
            defer allocator.free(left);
            defer allocator.free(right);
            const step0 = checkAnd(left, right, contextValuations);
            defer allocator.free(step0);
            contextValuations.clearAndFree();
            const step1 = checkNot(left, contextValuations);
            defer allocator.free(step1);
            contextValuations.clearAndFree();
            const step2 = checkNot(right, contextValuations);
            defer allocator.free(step2);
            contextValuations.clearAndFree();
            const step3 = checkAnd(step1, step2, contextValuations);
            defer allocator.free(step3);
            contextValuations.clearAndFree();
            return checkOr(step0, step3, contextValuations);
        },
        else => {
            return contextValuations.items;
        },
    }
}

fn checkNot(childValuations: []typeData.linearValuation, contextValuations: *ArrayList(typeData.linearValuation)) []typeData.linearValuation {
    var lower: ?u32 = null;
    var upper: ?u32 = null;
    var previous: ?u32 = null;
    while (true) {
        for (childValuations) |valuation| {
            if (valuation.until != infinity) {
                if (previous == null) {
                    if (lower == null) {
                        lower = valuation.until;
                    } else if (lower.? > valuation.until) {
                        lower = valuation.until;
                    }
                    if (upper == null) {
                        upper = valuation.from;
                    } else if (upper.? > valuation.from) {
                        upper = valuation.from;
                    }
                } else {
                    if (previous.? <= valuation.until) {
                        if (lower == null) {
                            lower = valuation.until;
                        } else if (lower.? > valuation.until) {
                            lower = valuation.until;
                        }
                    }
                    if (previous.? < valuation.from) {
                        if (upper == null) {
                            upper = valuation.from;
                        } else if (upper.? > valuation.from) {
                            upper = valuation.from;
                        }
                    }
                }
            } else {
                if (valuation.from != infinity and valuation.from != 1) {
                    if (previous == null) {
                        if (upper == null) {
                            upper = valuation.from;
                        } else if (upper.? > valuation.from) {
                            upper = valuation.from;
                        }
                    } else {
                        if (previous.? < valuation.from) {
                            if (upper == null) {
                                upper = valuation.from;
                            } else if (upper.? > valuation.from) {
                                upper = valuation.from;
                            }
                        }
                    }
                }
            }
        }
        if (lower != null and upper != null and previous == null) {
            if (upper.? != 1) {
                appendValuation(1, upper.? - 1, contextValuations) catch unreachable;
                previous = @max(lower.?, upper.?);
                lower = null;
                upper = null;
            } else {
                previous = @max(lower.?, upper.?);
                lower = null;
                upper = null;
            }
        } else if (lower != null and upper != null) {
            appendValuation(lower.? + 1, upper.? - 1, contextValuations) catch unreachable;
            previous = @max(lower.?, upper.?);
            lower = null;
            upper = null;
        } else if (lower != null and upper == null) {
            appendValuation(lower.? + 1, infinity, contextValuations) catch unreachable;
            break;
        } else if (lower == null and upper != null) {
            appendValuation(1, upper.? - 1, contextValuations) catch unreachable;
            break;
        } else if (childValuations.len == infinity) {
            appendValuation(1, infinity, contextValuations) catch unreachable;
            break;
        } else {
            break;
        }
    }
    var copy = allocator.alloc(typeData.linearValuation, contextValuations.items.len) catch unreachable;
    std.mem.copy(typeData.linearValuation, copy, contextValuations.items);
    return copy;
}

fn checkAnd(left: []typeData.linearValuation, right: []typeData.linearValuation, contextValuations: *ArrayList(typeData.linearValuation)) []typeData.linearValuation {
    for (left) |leftValuation| {
        for (right) |rightValuation| {
            if (leftValuation.until != infinity and rightValuation.until != infinity) {
                if (leftValuation.until >= rightValuation.from and leftValuation.until <= rightValuation.until) {
                    if (leftValuation.from < rightValuation.from) {
                        appendValuation(rightValuation.from, leftValuation.until, contextValuations) catch unreachable;
                    } else {
                        appendValuation(leftValuation.from, leftValuation.until, contextValuations) catch unreachable;
                    }
                } else if (rightValuation.until >= leftValuation.from and rightValuation.until <= leftValuation.until) {
                    if (rightValuation.from < leftValuation.from) {
                        appendValuation(leftValuation.from, rightValuation.until, contextValuations) catch unreachable;
                    } else {
                        appendValuation(rightValuation.from, rightValuation.until, contextValuations) catch unreachable;
                    }
                }
            } else if (leftValuation.until == infinity and rightValuation.until == infinity) {
                if (leftValuation.from == infinity and rightValuation.from == infinity) {
                    appendValuation(infinity, infinity, contextValuations) catch unreachable;
                } else if (leftValuation.from != infinity and rightValuation.from != infinity) {
                    appendValuation(@max(leftValuation.from, rightValuation.from), infinity, contextValuations) catch unreachable;
                }
            } else if (leftValuation.from != infinity or rightValuation.from != infinity) {
                if (leftValuation.until == infinity) {
                    if (rightValuation.until >= leftValuation.from) {
                        appendValuation(@max(leftValuation.from, rightValuation.from), rightValuation.until, contextValuations) catch unreachable;
                    }
                } else if (rightValuation.until == infinity) {
                    if (leftValuation.until >= rightValuation.from) {
                        appendValuation(@max(leftValuation.from, rightValuation.from), leftValuation.until, contextValuations) catch unreachable;
                    }
                }
            }
        }
    }
    conctenate(contextValuations);
    var copy = allocator.alloc(typeData.linearValuation, contextValuations.items.len) catch unreachable;
    std.mem.copy(typeData.linearValuation, copy, contextValuations.items);
    return copy;
}

fn checkOr(left: []typeData.linearValuation, right: []typeData.linearValuation, contextValuations: *ArrayList(typeData.linearValuation)) []typeData.linearValuation {
    contextValuations.appendSlice(left) catch unreachable;
    contextValuations.appendSlice(right) catch unreachable;
    conctenate(contextValuations);
    var copy = allocator.alloc(typeData.linearValuation, contextValuations.items.len) catch unreachable;
    std.mem.copy(typeData.linearValuation, copy, contextValuations.items);
    return copy;
}

fn checkNext(childValuations: []typeData.linearValuation, contextValuations: *ArrayList(typeData.linearValuation)) []typeData.linearValuation {
    for (childValuations) |valuation| {
        if (valuation.until == infinity and valuation.from != infinity) {
            appendValuation(@max(valuation.from - 1, 1), valuation.until, contextValuations) catch unreachable;
        } else if (valuation.until != infinity and valuation.from != infinity and valuation.until != 1) {
            appendValuation(@max(valuation.from - 1, 1), valuation.until - 1, contextValuations) catch unreachable;
        } else if (valuation.until == infinity and valuation.from == infinity) {
            appendValuation(valuation.from, valuation.until, contextValuations) catch unreachable;
        }
    }
    var copy = allocator.alloc(typeData.linearValuation, contextValuations.items.len) catch unreachable;
    std.mem.copy(typeData.linearValuation, copy, contextValuations.items);
    return copy;
}

fn checkFurure(childValuations: []typeData.linearValuation, contextValuations: *ArrayList(typeData.linearValuation)) []typeData.linearValuation {
    var until: ?u32 = null;
    for (childValuations) |valuation| {
        if (until == null or (valuation.until > until.? and until.? != infinity) or valuation.until == infinity) {
            until = valuation.until;
        }
    }
    if (until != null) {
        appendValuation(1, until.?, contextValuations) catch unreachable;
    }
    var copy = allocator.alloc(typeData.linearValuation, contextValuations.items.len) catch unreachable;
    std.mem.copy(typeData.linearValuation, copy, contextValuations.items);
    return copy;
}

fn checkGlobal(childValuations: []typeData.linearValuation, contextValuations: *ArrayList(typeData.linearValuation)) []typeData.linearValuation {
    for (childValuations) |valuation| {
        if (valuation.until == infinity) {
            appendValuation(valuation.from, valuation.until, contextValuations) catch unreachable;
        }
    }
    var copy = allocator.alloc(typeData.linearValuation, contextValuations.items.len) catch unreachable;
    std.mem.copy(typeData.linearValuation, copy, contextValuations.items);
    return copy;
}

fn checkUntil(fromValuations: []typeData.linearValuation, untilValuations: []typeData.linearValuation, contextValuations: *ArrayList(typeData.linearValuation)) []typeData.linearValuation {
    if (untilValuations.len != infinity) {
        for (fromValuations) |fromValuation| {
            for (untilValuations) |untilValuation| {
                if (fromValuation.until >= untilValuation.from - 1) {
                    appendValuation(fromValuation.from, untilValuation.until, contextValuations) catch unreachable;
                }
            }
        }
        for (untilValuations) |untilValuation| {
            contextValuations.append(untilValuation) catch unreachable;
        }
    }
    conctenate(contextValuations);
    var copy = allocator.alloc(typeData.linearValuation, contextValuations.items.len) catch unreachable;
    std.mem.copy(typeData.linearValuation, copy, contextValuations.items);
    return copy;
}

fn checkRelease(pendingValuations: []typeData.linearValuation, releaseValuations: []typeData.linearValuation, contextValuations: *ArrayList(typeData.linearValuation)) []typeData.linearValuation {
    for (pendingValuations) |pendingValuation| {
        for (releaseValuations) |releaseValuation| {
            if (pendingValuation.until >= releaseValuation.from) {
                if (pendingValuation.until < releaseValuation.until) {
                    appendValuation(pendingValuation.from, pendingValuation.until, contextValuations) catch unreachable;
                } else {
                    appendValuation(pendingValuation.from, releaseValuation.until, contextValuations) catch unreachable;
                }
            }
        }
    }
    for (pendingValuations) |pendingValuation| {
        if (pendingValuation.until == infinity) {
            appendValuation(pendingValuation.from, pendingValuation.until, contextValuations) catch unreachable;
        }
    }
    conctenate(contextValuations);
    var copy = allocator.alloc(typeData.linearValuation, contextValuations.items.len) catch unreachable;
    std.mem.copy(typeData.linearValuation, copy, contextValuations.items);
    return copy;
}

fn checkWeakUntil(fromValuations: []typeData.linearValuation, untilValuations: []typeData.linearValuation, contextValuations: *ArrayList(typeData.linearValuation)) []typeData.linearValuation {
    for (fromValuations) |fromValuation| {
        for (untilValuations) |untilValuation| {
            if (fromValuation.until >= untilValuation.from - 1) {
                appendValuation(fromValuation.from, untilValuation.until, contextValuations) catch unreachable;
            }
        }
    }
    for (fromValuations) |fromValuation| {
        if (fromValuation.until == infinity) {
            appendValuation(fromValuation.from, fromValuation.until, contextValuations) catch unreachable;
        }
    }
    for (untilValuations) |untilValuation| {
        appendValuation(untilValuation.from, untilValuation.until, contextValuations) catch unreachable;
    }
    conctenate(contextValuations);
    var copy = allocator.alloc(typeData.linearValuation, contextValuations.items.len) catch unreachable;
    std.mem.copy(typeData.linearValuation, copy, contextValuations.items);
    return copy;
}
