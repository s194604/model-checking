const std = @import("std");
const formatter = @import("formatter.zig");
const parser = @import("parser.zig");
const labeler = @import("labeler.zig");
const algorithm = @import("labeling_algorithm.zig");
const typeData = @import("types.zig");
const ArrayList = std.ArrayList;
const Queue = std.atomic.Queue;
const sentinel = @import("constants.zig").sentinel;
const charType = @import("constants.zig").charType;
const allocator = std.heap.page_allocator;

pub fn main() void {
    const w = 12;
    const r: [*:sentinel]const charType = &[_:sentinel]charType{ '(', '1', ',', ' ', '2', ')', ' ', '(', '2', ',', ' ', '3', ')', ' ', '(', '3', ',', ' ', '4', ')', ' ', '(', '4', ',', ' ', '5', ')' };
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', ' ', ',', '2', ' ', ',', '3', '.', ' ', 'q', ':', '2', ',', ' ', '3', ',', ' ', '4', ',', ' ', '5', '.' };
    const expression: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', '∧', 'q' };
    var tokenContainer = ArrayList(Queue(typeData.Token).Node).init(allocator);
    const states = algorithm.checkModel(w, formatter.parseRelations(r), formatter.parseValuations(w, v), labeler.labelTree(parser.parseExpressionToTree(expression, &tokenContainer, typeData.logicalSystem.PROPOSITIONAL), tokenContainer.items.len));
    std.debug.print("Valid states: {?any}\n", .{states});
}

const formatter_tests = @import("tests/formatter.zig");
const propositional_tests = @import("tests/propositional_logic.zig");
const modal_tests = @import("tests/modal_logic.zig");
const temporal_tests = @import("tests/temporal_logic.zig");

test {
    std.testing.refAllDecls(formatter_tests);
    std.testing.refAllDecls(propositional_tests);
    std.testing.refAllDecls(modal_tests);
    std.testing.refAllDecls(temporal_tests);
}
