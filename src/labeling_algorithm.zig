const std = @import("std");
const typeData = @import("types.zig");
const Vector = std.meta.Vector;
const ArrayList = std.ArrayList;
const allocator = std.heap.page_allocator;
const charType = @import("constants.zig").charType;

pub fn checkModel(w: u32, r: *const ArrayList(Vector(2, u32)), v: *const ArrayList(ArrayList(charType)), expression: *const typeData.Node) ?[]u32 {
    var validNodes = ArrayList(u32).init(allocator);
    var labelList = ArrayList(ArrayList(u32)).init(allocator);
    var i: u32 = 0;
    while (i < w) : (i += 1) {
        labelList.append(ArrayList(u32).init(allocator)) catch unreachable;
    }
    const model = typeData.Kripke{
        .w = w,
        .r = r,
        .v = v,
    };
    var stateIndex: u32 = 1;
    while (stateIndex <= w) : (stateIndex += 1) {
        if (check(&model, expression, stateIndex, &labelList)) {
            validNodes.append(stateIndex) catch unreachable;
        }
    }
    if (validNodes.items.len > 0) {
        return validNodes.items;
    }
    return null;
}

fn check(model: *const typeData.Kripke, expression: *const typeData.Node, state: u32, labels: *ArrayList(ArrayList(u32))) bool {
    var id: u32 = undefined;
    var operator: typeData.Type = undefined;
    switch (expression.*) {
        .boolean => |b| {
            return b.value;
        },
        .proposition => |p| {
            id = p.label.?;
            operator = typeData.Type.PROPOSITION;
        },
        .unary => |u| {
            id = u.label.?;
            operator = u.type;
        },
        .binary => |b| {
            id = b.label.?;
            operator = b.type;
        },
    }
    if (checkLabel(id, state, labels)) {
        return true;
    }
    switch (operator) {
        typeData.Type.PROPOSITION => {
            if (checkValuations(model, expression, state)) {
                setLabel(id, state, labels);
                return true;
            }
            return false;
        },
        typeData.Type.MODAL_POSSIBLE => {
            if (checkPossible(model, expression, state, labels)) {
                setLabel(id, state, labels);
                return true;
            }
            return false;
        },
        typeData.Type.MODAL_ALWAYS => {
            if (checkAlways(model, expression, state, labels)) {
                setLabel(id, state, labels);
                return true;
            }
            return false;
        },
        typeData.Type.NOT => {
            if (!check(model, expression.unary.child, state, labels)) {
                setLabel(id, state, labels);
                return true;
            }
            return false;
        },
        typeData.Type.AND => {
            if (check(model, expression.binary.left, state, labels) and check(model, expression.binary.right, state, labels)) {
                setLabel(id, state, labels);
                return true;
            }
            return false;
        },
        typeData.Type.OR => {
            if (check(model, expression.binary.left, state, labels) or check(model, expression.binary.right, state, labels)) {
                setLabel(id, state, labels);
                return true;
            }
            return false;
        },
        typeData.Type.IF => {
            const left = check(model, expression.binary.left, state, labels);
            const right = check(model, expression.binary.right, state, labels);
            if (!left and !right or !left and right or left and right) {
                setLabel(id, state, labels);
                return true;
            }
            return false;
        },
        typeData.Type.IFF => {
            const left = check(model, expression.binary.left, state, labels);
            const right = check(model, expression.binary.right, state, labels);
            if (!left and !right or left and right) {
                setLabel(id, state, labels);
                return true;
            }
            return false;
        },
        else => {
            return false;
        },
    }
}

fn checkPossible(model: *const typeData.Kripke, expression: *const typeData.Node, state: u32, labels: *ArrayList(ArrayList(u32))) bool {
    for (model.r.*.items) |relation| {
        if (relation[0] == state) {
            if (check(model, expression.unary.child, relation[1], labels)) {
                return true;
            }
        }
    }
    return false;
}

fn checkAlways(model: *const typeData.Kripke, expression: *const typeData.Node, state: u32, labels: *ArrayList(ArrayList(u32))) bool {
    for (model.r.*.items) |relation| {
        if (relation[0] == state) {
            if (!check(model, expression.unary.child, relation[1], labels)) {
                return false;
            }
        }
    }
    return true;
}

fn setLabel(id: u32, state: usize, labels: *ArrayList(ArrayList(u32))) void {
    labels.items[state - 1].append(id) catch unreachable;
}

fn checkLabel(id: u32, state: usize, labels: *ArrayList(ArrayList(u32))) bool {
    for (labels.items[state - 1].items) |label| {
        if (label == id) {
            return true;
        }
    }
    return false;
}

fn checkValuations(model: *const typeData.Kripke, proposition: *const typeData.Node, state: usize) bool {
    for (model.v.*.items[state - 1].items) |valuation| {
        if (valuation == proposition.proposition.value) {
            return true;
        }
    }
    return false;
}
