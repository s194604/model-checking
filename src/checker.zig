const std = @import("std");
const formatter = @import("formatter.zig");
const parser = @import("parser.zig");
const labeler = @import("labeler.zig");
const typeData = @import("types.zig");
const ArrayList = std.ArrayList;
const Queue = std.atomic.Queue;
const Vector = std.meta.Vector;
const sentinel = @import("constants.zig").sentinel;
const charType = @import("constants.zig").charType;
const allocator = std.heap.page_allocator;

pub extern "printer" fn print(input: charType) void;
pub extern "nodePusher" fn pushNode(label: [*]const u8, length: u32, isValid: bool) void;
pub extern "edgePusher" fn pushEdge(from: u32, to: u32) void;

pub export fn checkPropositional(w: u32, r: [*:sentinel]const charType, v: [*:sentinel]const charType, expression: [*:sentinel]const charType, output: [*]u32) void {
    const algorithm = @import("labeling_algorithm.zig");
    var tokenContainer = ArrayList(Queue(typeData.Token).Node).init(allocator);
    const r_processed = formatter.parseRelations(r);
    const v_processed = formatter.parseValuations(w, v);
    const states = algorithm.checkModel(w, r_processed, v_processed, labeler.labelTree(parser.parseExpressionToTree(expression, &tokenContainer, typeData.logicalSystem.PROPOSITIONAL), tokenContainer.items.len));
    visualizer(r_processed, v_processed, states);
    outputter(output, states);
}

pub export fn checkModal(w: u32, r: [*:sentinel]const charType, v: [*:sentinel]const charType, expression: [*:sentinel]const charType, output: [*]u32) void {
    const algorithm = @import("labeling_algorithm.zig");
    var tokenContainer = ArrayList(Queue(typeData.Token).Node).init(allocator);
    const states = algorithm.checkModel(w, formatter.parseRelations(r), formatter.parseValuations(w, v), labeler.labelTree(parser.parseExpressionToTree(expression, &tokenContainer, typeData.logicalSystem.MODAL), tokenContainer.items.len));
    outputter(output, states);
}

pub export fn checkTemporal(v: [*:sentinel]const charType, expression: [*:sentinel]const charType, output: [*][*]u32) u32 {
    const algorithm = @import("ltl_algorithm.zig");
    var tokenContainer = ArrayList(Queue(typeData.Token).Node).init(allocator);
    const states = algorithm.checkModel(formatter.parseLinearValuations(v), parser.parseExpressionToTree(expression, &tokenContainer, typeData.logicalSystem.TEMPORAL));
    const outputA = allocator.alloc(u32, states.len) catch unreachable;
    const outputB = allocator.alloc(u32, states.len) catch unreachable;
    for (states) |linearValuation, i| {
        outputA[i] = linearValuation.from;
        outputB[i] = linearValuation.until;
    }
    output[0] = outputA.ptr;
    output[1] = outputB.ptr;
    return @intCast(u32, states.len);
}

fn outputter(output: [*]u32, states: ?[]u32) void {
    if (states == null) {
        output[0] = 0;
        return;
    }
    for (states.?) |state, i| {
        output[i] = state;
    }
    output[states.?.len] = 0;
}

fn visualizer(r: *const ArrayList(Vector(2, u32)), v: *const ArrayList(ArrayList(charType)), o: ?[]u32) void {
    var label = ArrayList(u8).init(allocator);
    for (v.items) |item, i| {
        label.clearAndFree();
        for (item.items) |char, j| {
            label.append(@intCast(u8, char)) catch unreachable;
            if (j != item.items.len - 1) {
                label.append(',') catch unreachable;
                label.append(' ') catch unreachable;
            }
        }
        if (o == null) {
            pushNode(label.items.ptr, label.items.len, false);
        } else {
            var isValid = false;
            for (o.?) |state| {
                if (i + 1 == state) {
                    pushNode(label.items.ptr, label.items.len, true);
                    isValid = true;
                }
            }
            if (!isValid) {
                pushNode(label.items.ptr, label.items.len, false);
            }
        }
    }
    for (r.items) |item| {
        pushEdge(item[0], item[1]);
    }
}
