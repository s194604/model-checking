const std = @import("std");
const typeData = @import("types.zig");
const Vector = std.meta.Vector;
const ArrayList = std.ArrayList;
const Queue = std.atomic.Queue;
const HashMap = std.AutoArrayHashMap;
const allocator = std.heap.page_allocator;
const sortAscending = std.sort.sort;
const sentinel = @import("constants.zig").sentinel;
const charType = @import("constants.zig").charType;
const infinity = @import("constants.zig").infinity;

var relations = ArrayList(Vector(2, u32)).init(allocator);
var valuations = ArrayList(ArrayList(charType)).init(allocator);
var tokens = Queue(typeData.Token).init();
var multiChars = ArrayList(ArrayList(charType)).init(allocator);
var linearValuations = HashMap(charType, ArrayList(typeData.linearValuation)).init(allocator);

pub fn parseRelations(chars: [*:sentinel]const charType) *const ArrayList(Vector(2, u32)) {
    relations.clearAndFree();
    var i: usize = 0;
    var isLeft: bool = undefined;
    var left: u32 = undefined;
    var right: u32 = undefined;
    while (chars[i] != sentinel) : (i += 1) {
        if (isDigit(chars[i])) {
            if (i > 0) {
                if (isDigit(chars[i - 1])) {
                    if (isLeft) {
                        left = left * 10 + @intCast(u32, charDigitToNumDigit(chars[i]));
                    } else {
                        right = right * 10 + @intCast(u32, charDigitToNumDigit(chars[i]));
                    }
                } else {
                    if (isLeft) {
                        left = @intCast(u32, charDigitToNumDigit(chars[i]));
                    } else {
                        right = @intCast(u32, charDigitToNumDigit(chars[i]));
                    }
                }
            } else {
                //TODO: Error handling
            }
        } else if (chars[i] == '(') {
            left = 0;
            right = 0;
            isLeft = true;
        } else if ((chars[i] == ',' or chars[i] == ';') and isLeft) {
            isLeft = false;
        } else if (chars[i] == ')') {
            relations.append(.{ left, right }) catch unreachable;
        }
    }
    return &relations;
}

pub fn parseValuations(w: u32, chars: [*:sentinel]const charType) *const ArrayList(ArrayList(charType)) {
    valuations.clearAndFree();
    var i: usize = 0;
    while (i < w) : (i += 1) {
        valuations.append(ArrayList(charType).init(allocator)) catch unreachable;
    }
    var currentChar: charType = undefined;
    var jaggedIndex: usize = undefined;
    i = 0;
    while (chars[i] != sentinel) : (i += 1) {
        if (isDigit(chars[i])) {
            if (i > 0) {
                if (isDigit(chars[i - 1])) {
                    jaggedIndex = jaggedIndex * 10 + @intCast(u32, charDigitToNumDigit(chars[i]));
                } else {
                    jaggedIndex = @intCast(u32, charDigitToNumDigit(chars[i]));
                }
                if (!isDigit(chars[i + 1])) {
                    valuations.items[jaggedIndex - 1].append(currentChar) catch unreachable;
                }
            } else {
                //TODO: Error handling
            }
        } else if (isLowerCase(chars[i])) {
            if (isMultiCharValid(chars[i + 1])) {
                currentChar = addMultiCharToken(chars, &i);
            } else {
                currentChar = chars[i];
            }
        }
    }
    return &valuations;
}

pub fn tokenizeExpression(chars: [*:sentinel]const charType, tokenContainer: *ArrayList(Queue(typeData.Token).Node), context: typeData.logicalSystem) *Queue(typeData.Token) {
    var i: usize = 0;
    var tokenType: ?typeData.Type = undefined;
    while (chars[i] != sentinel) : (i += 1) {
        if (chars[i] == ' ') {
            continue;
        } else if (isLowerCase(chars[i])) {
            if (isMultiCharValid(chars[i + 1])) {
                tokenContainer.append(Queue(typeData.Token).Node{ .data = typeData.Token{
                    .type = typeData.Type.PROPOSITION,
                    .value = addMultiCharToken(chars, &i),
                } }) catch unreachable;
                tokens.put(&tokenContainer.items[tokenContainer.items.len - 1]);
                continue;
            }
            tokenContainer.append(Queue(typeData.Token).Node{ .data = typeData.Token{
                .type = typeData.Type.PROPOSITION,
                .value = chars[i],
            } }) catch unreachable;
            tokens.put(&tokenContainer.items[tokenContainer.items.len - 1]);
            continue;
        }
        tokenType = typeData.characterType(chars[i], context);
        if (tokenType == null) {
            //TODO: error handling
        }
        tokenContainer.append(Queue(typeData.Token).Node{ .data = typeData.Token{
            .type = tokenType.?,
            .value = chars[i],
        } }) catch unreachable;
        tokens.put(&tokenContainer.items[tokenContainer.items.len - 1]);
    }
    return &tokens;
}

fn addMultiCharToken(chars: [*:sentinel]const charType, index: *usize) i32 {
    var multiId: i32 = undefined;
    var multiChar = ArrayList(charType).init(allocator);
    while (isMultiCharValid(chars[index.*])) : (index.* += 1) {
        if (chars[index.* + 1] == sentinel) {
            if (isUpperCase(chars[index.*]) or chars[index.*] == '_') {
                //TODO: invalid ending character error message
            }
        }
        multiChar.append(chars[index.*]) catch unreachable;
    }
    index.* -= 1;
    var equal = false;
    for (multiChars.items) |item, i| {
        if (item.items.len == multiChar.items.len) {
            equal = true;
            for (item.items) |char, j| {
                if (char != multiChar.items[j]) {
                    equal = false;
                }
            }
        }
        if (equal) {
            multiId = -@intCast(i32, i + 1);
            return multiId;
        }
    }
    multiChars.append(multiChar) catch unreachable;
    multiId = -@intCast(i32, multiChars.items.len);
    return multiId;
}

pub fn parseLinearValuations(chars: [*:sentinel]const charType) *const HashMap(charType, ArrayList(typeData.linearValuation)) {
    linearValuations.clearAndFree();
    var i: usize = 0;
    var currentChar: charType = undefined;
    var currentState: u32 = undefined;
    var isUntil: bool = false;
    var fromUntil: u32 = undefined;
    while (chars[i] != sentinel) : (i += 1) {
        if (isDigit(chars[i])) {
            if (i > 0) {
                if (isDigit(chars[i - 1])) {
                    currentState = currentState * 10 + @intCast(u32, charDigitToNumDigit(chars[i]));
                } else {
                    currentState = @intCast(u32, charDigitToNumDigit(chars[i]));
                }
                if (!isDigit(chars[i + 1])) {
                    if (chars[i + 1] == 'A') {
                        appendValuation(currentState, infinity, linearValuations.getPtr(currentChar).?) catch unreachable;
                        i += 1;
                    } else if (chars[i + 1] == 'U') {
                        isUntil = true;
                        fromUntil = currentState;
                        i += 1;
                    } else {
                        if (isUntil) {
                            if (fromUntil <= currentState or currentState == infinity) {
                                appendValuation(fromUntil, currentState, linearValuations.getPtr(currentChar).?) catch unreachable;
                            } else {
                                //TODO: error handling
                            }
                        } else {
                            appendValuation(currentState, currentState, linearValuations.getPtr(currentChar).?) catch unreachable;
                        }
                    }
                }
            } else {
                //TODO: Error handling
            }
        } else if (chars[i] == '∞') {
            currentState = infinity;
            if (isUntil) {
                appendValuation(fromUntil, currentState, linearValuations.getPtr(currentChar).?) catch unreachable;
            } else {
                appendValuation(currentState, currentState, linearValuations.getPtr(currentChar).?) catch unreachable;
            }
        } else if (isLowerCase(chars[i])) {
            if (isMultiCharValid(chars[i + 1])) {
                currentChar = addMultiCharToken(chars, &i);
            } else {
                currentChar = chars[i];
            }
            if (!linearValuations.contains(currentChar)) {
                linearValuations.put(currentChar, ArrayList(typeData.linearValuation).init(allocator)) catch unreachable;
            }
        }
    }
    concatenateLinearValuations();
    return &linearValuations;
}

pub fn appendValuation(from: u32, until: u32, contextValuations: *ArrayList(typeData.linearValuation)) !void {
    try contextValuations.append(typeData.linearValuation{
        .from = from,
        .until = until,
    });
}

fn concatenateLinearValuations() void {
    const keys = linearValuations.keys();
    for (keys) |key| {
        const array = linearValuations.getPtr(key).?;
        concatenateLinearValuation(array);
    }
}

pub fn concatenateLinearValuation(array: *ArrayList(typeData.linearValuation)) void {
    if (array.*.items.len == 0) {
        return;
    }
    var i: usize = 0;
    var j: usize = 0;
    var iValuation: typeData.linearValuation = undefined;
    var jValuation: typeData.linearValuation = undefined;
    while (true) {
        if (i != j) {
            iValuation = array.*.items[i];
            jValuation = array.*.items[j];
            if (iValuation.from == infinity or jValuation.from == infinity) {
                if (jValuation.from == infinity) {
                    if (iValuation.until == infinity) {
                        _ = array.*.swapRemove(j);
                        if (i >= array.*.items.len) {
                            break;
                        } else if (j < array.*.items.len - 1) {
                            continue;
                        }
                    }
                } else if (iValuation.from == infinity) {
                    if (jValuation.until == infinity) {
                        array.*.items[i].from = jValuation.from;
                        _ = array.*.swapRemove(j);
                        if (i >= array.*.items.len) {
                            break;
                        } else if (j < array.*.items.len - 1) {
                            continue;
                        }
                    }
                }
            } else if (iValuation.until == infinity or jValuation.until == infinity) {
                if (jValuation.until == infinity) {
                    if (iValuation.until >= jValuation.from - 1 or iValuation.until == infinity) {
                        array.*.items[i].from = @min(iValuation.from, jValuation.from);
                        array.*.items[i].until = jValuation.until;
                        _ = array.*.swapRemove(j);
                        if (i >= array.*.items.len) {
                            break;
                        } else if (j < array.*.items.len - 1) {
                            continue;
                        }
                    }
                } else if (iValuation.until == infinity) {
                    if (jValuation.until >= iValuation.from - 1 or jValuation.until == infinity) {
                        array.*.items[i].from = @min(iValuation.from, jValuation.from);
                        _ = array.*.swapRemove(j);
                        if (i >= array.*.items.len) {
                            break;
                        } else if (j < array.*.items.len - 1) {
                            continue;
                        }
                    }
                }
            } else if (iValuation.until >= jValuation.from - 1 and iValuation.until <= jValuation.until) {
                array.*.items[i].from = @min(iValuation.from, jValuation.from);
                array.*.items[i].until = jValuation.until;
                _ = array.*.swapRemove(j);
                if (i >= array.*.items.len) {
                    break;
                } else if (j < array.*.items.len - 1) {
                    continue;
                }
            } else if (jValuation.until >= iValuation.from - 1 and jValuation.until <= iValuation.until) {
                array.*.items[i].from = @min(iValuation.from, jValuation.from);
                _ = array.*.swapRemove(j);
                if (i >= array.*.items.len) {
                    break;
                } else if (j < array.*.items.len - 1) {
                    continue;
                }
            }
        }
        if (j < array.*.items.len - 1) {
            j += 1;
        } else {
            j = 0;
            if (i < array.*.items.len - 1) {
                i += 1;
            } else {
                break;
            }
        }
    }
    sortAscending(typeData.linearValuation, array.*.items, void, lessThanLinearValuation);
}

fn removeLinearValuationDuplicates(array: *ArrayList(typeData.linearValuation)) void {
    var i: usize = 0;
    var j: usize = 0;
    while (true) {
        if (i != j) {
            if (array.*.items[i].from == array.*.items[j].from and array.*.items[i].until == array.*.items[j].until) {
                _ = array.*.swapRemove(j);
                j -= 1;
            }
        }
        if (j < array.*.items.len - 1) {
            j += 1;
        } else {
            j = 0;
            if (i < array.*.items.len - 1) {
                i += 1;
            } else {
                break;
            }
        }
    }
}

fn lessThanLinearValuation(comptime context: @TypeOf(void), lhs: typeData.linearValuation, rhs: typeData.linearValuation) bool {
    _ = context;
    if (lhs.from == infinity) {
        return false;
    } else if (rhs.from == infinity) {
        return true;
    }
    return lhs.from < rhs.from;
}

fn isDigit(char: charType) bool {
    return char >= '0' and char <= '9';
}

fn charDigitToNumDigit(char: charType) charType {
    return char - '0';
}

fn isMultiCharValid(char: charType) bool {
    return isLetter(char) or char == '_' or char == '-';
}

fn isLetter(char: charType) bool {
    return isUpperCase(char) or isLowerCase(char);
}

fn isUpperCase(char: charType) bool {
    return isUpperCaseLetter(char) or isUpperCaseGreekLetter(char);
}

fn isLowerCase(char: charType) bool {
    return isLowerCaseLetter(char) or isLowerCaseGreekLetter(char);
}

fn isUpperCaseLetter(char: charType) bool {
    return char >= 0x0041 and char <= 0x005A;
}

fn isLowerCaseLetter(char: charType) bool {
    return char >= 0x0061 and char <= 0x007A;
}

fn isUpperCaseGreekLetter(char: charType) bool {
    return char >= 0x0391 and char <= 0x03A9;
}

fn isLowerCaseGreekLetter(char: charType) bool {
    return char >= 0x03B1 and char <= 0x03C9;
}
