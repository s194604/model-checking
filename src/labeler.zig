const std = @import("std");
const typeData = @import("types.zig");
const ArrayList = std.ArrayList;
const allocator = std.heap.page_allocator;

pub fn labelTree(root: *typeData.Node, length: usize) *const typeData.Node {
    var currentLabel: u32 = 2;
    var labelList = ArrayList(*typeData.Node).init(allocator);
    labelList.ensureTotalCapacity(length) catch unreachable;
    if (label(root, typeData.TokenType.BASE, &labelList, &currentLabel)) {
        labelList.clearRetainingCapacity();
        var possibleUnary = true;
        var possibleBinary = true;
        while (possibleUnary or possibleBinary) {
            if (possibleUnary) {
                possibleUnary = label(root, typeData.TokenType.UNARY, &labelList, &currentLabel);
            } else if (possibleBinary) {
                possibleBinary = label(root, typeData.TokenType.BINARY, &labelList, &currentLabel);
                possibleUnary = true;
            }
            labelList.clearRetainingCapacity();
        }
    }
    return root;
}

fn label(node: *typeData.Node, operator: typeData.TokenType, list: *ArrayList(*typeData.Node), x: *u32) bool {
    switch (node.*) {
        .boolean => |*bo| {
            if (operator == typeData.TokenType.BASE and bo.*.label == null) {
                addLabelBool(node);
                return true;
            }
        },
        .proposition => |*prop| {
            if (operator == typeData.TokenType.BASE and prop.*.label == null) {
                addLabelProp(node, list, x);
                return true;
            }
        },
        .unary => |*un| {
            var childLabel: ?u32 = undefined;
            switch (un.*.child.*) {
                .boolean => |b| childLabel = b.label,
                .proposition => |p| childLabel = p.label,
                .unary => |u| childLabel = u.label,
                .binary => |b| childLabel = b.label,
            }
            if (childLabel == null) {
                return label(un.*.child, operator, list, x);
            } else if (operator == typeData.TokenType.UNARY and un.*.label == null) {
                addLabelUnary(node, list, x);
                return true;
            }
        },
        .binary => |*bin| {
            var leftLabel: ?u32 = undefined;
            var rightLabel: ?u32 = undefined;
            switch (bin.*.left.*) {
                .boolean => |b| leftLabel = b.label,
                .proposition => |p| leftLabel = p.label,
                .unary => |u| leftLabel = u.label,
                .binary => |b| leftLabel = b.label,
            }
            switch (bin.*.right.*) {
                .boolean => |b| rightLabel = b.label,
                .proposition => |p| rightLabel = p.label,
                .unary => |u| rightLabel = u.label,
                .binary => |b| rightLabel = b.label,
            }
            if (leftLabel == null and rightLabel != null) {
                return label(bin.*.left, operator, list, x);
            } else if (leftLabel != null and rightLabel == null) {
                return label(bin.*.right, operator, list, x);
            } else if (leftLabel == null and rightLabel == null) {
                const left = label(bin.*.left, operator, list, x);
                const right = label(bin.*.right, operator, list, x);
                return left or right;
            } else if (operator == typeData.TokenType.BINARY and bin.*.label == null) {
                addLabelBinary(node, list, x);
                return true;
            }
        },
    }
    return false;
}

fn addLabelBool(node: *typeData.Node) void {
    if (node.*.boolean.value == false) {
        node.*.boolean.label = 0;
    } else {
        node.*.boolean.label = 1;
    }
}

fn addLabelProp(node: *typeData.Node, list: *ArrayList(*typeData.Node), x: *u32) void {
    for (list.*.items) |item| {
        if (node.*.proposition.value == item.*.proposition.value) {
            node.*.proposition.label = item.*.proposition.label;
            return;
        }
    }
    node.*.proposition.label = x.*;
    list.append(node) catch unreachable;
    x.* += 1;
}

fn addLabelUnary(node: *typeData.Node, list: *ArrayList(*typeData.Node), x: *u32) void {
    for (list.*.items) |item| {
        if (node.*.unary.type == item.*.unary.type and node.*.unary.child.*.getLabel() == item.*.unary.child.*.getLabel()) {
            node.*.unary.label = item.*.unary.label;
            return;
        }
    }
    node.*.unary.label = x.*;
    list.append(node) catch unreachable;
    x.* += 1;
}

fn addLabelBinary(node: *typeData.Node, list: *ArrayList(*typeData.Node), x: *u32) void {
    for (list.*.items) |item| {
        if (node.*.binary.type == item.*.binary.type) {
            if (node.*.binary.type.isCommutative()) {
                if (node.*.binary.left.*.getLabel() == item.*.binary.left.*.getLabel() and node.*.binary.right.*.getLabel() == item.*.binary.right.*.getLabel() or node.*.binary.left.*.getLabel() == item.*.binary.right.*.getLabel() and node.*.binary.right.*.getLabel() == item.*.binary.left.*.getLabel()) {
                    node.*.binary.label = item.*.binary.label;
                    return;
                }
            } else {
                if (node.*.binary.left.*.getLabel() == item.*.binary.left.*.getLabel() and node.*.binary.right.*.getLabel() == item.*.binary.right.*.getLabel()) {
                    node.*.binary.label = item.*.binary.label;
                    return;
                }
            }
        }
    }
    node.*.binary.label = x.*;
    list.append(node) catch unreachable;
    x.* += 1;
}
