const std = @import("std");
const testing = std.testing;
const checker = @import("../checker.zig");
const sentinel = @import("../constants.zig").sentinel;
const charType = @import("../constants.zig").charType;
const allocator = std.heap.page_allocator;

test "possibility0" {
    const w = 12;
    const r: [*:sentinel]const charType = &[_:sentinel]charType{ '(', '1', ',', ' ', '2', ')', ' ', '(', '1', ',', ' ', '3', ')', ' ', '(', '1', ',', ' ', '4', ')', ' ', '(', '4', ',', ' ', '5', ')' };
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ '◇', 'p' };
    var o: []u32 = try allocator.alloc(u32, w + 1);
    checker.checkModal(w, r, v, e, o[0..w]);
    try testing.expectEqual(@intCast(u32, 0), o[0]);
}

test "possibility1" {
    const w = 12;
    const r: [*:sentinel]const charType = &[_:sentinel]charType{ '(', '1', ',', ' ', '1', ')', ' ', '(', '1', ',', ' ', '2', ')', ' ', '(', '1', ',', ' ', '3', ')', ' ', '(', '1', ',', ' ', '4', ')', ' ', '(', '4', ',', ' ', '5', ')' };
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ '◇', 'p' };
    var o: []u32 = try allocator.alloc(u32, w + 1);
    checker.checkModal(w, r, v, e, o[0..w]);
    try testing.expectEqual(@intCast(u32, 1), o[0]);
    try testing.expectEqual(@intCast(u32, 0), o[1]);
}

test "possibility2" {
    const w = 12;
    const r: [*:sentinel]const charType = &[_:sentinel]charType{ '(', '1', ',', ' ', '2', ')', ' ', '(', '1', ',', ' ', '3', ')', ' ', '(', '1', ',', ' ', '4', ')', ' ', '(', '4', ',', ' ', '5', ')' };
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '2', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ '◇', '◇', 'p' };
    var o: []u32 = try allocator.alloc(u32, w + 1);
    checker.checkModal(w, r, v, e, o[0..w]);
    try testing.expectEqual(@intCast(u32, 0), o[0]);
}

test "possibility3" {
    const w = 12;
    const r: [*:sentinel]const charType = &[_:sentinel]charType{ '(', '1', ',', ' ', '2', ')', ' ', '(', '1', ',', ' ', '3', ')', ' ', '(', '1', ',', ' ', '4', ')', ' ', '(', '4', ',', ' ', '5', ')' };
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '5', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ '◇', '◇', 'p' };
    var o: []u32 = try allocator.alloc(u32, w + 1);
    checker.checkModal(w, r, v, e, o[0..w]);
    try testing.expectEqual(@intCast(u32, 1), o[0]);
    try testing.expectEqual(@intCast(u32, 0), o[1]);
}

test "necessity0" {
    const w = 2;
    const r: [*:sentinel]const charType = &[_:sentinel]charType{ '(', '1', ',', ' ', '2', ')' };
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ '□', 'p' };
    var o: []u32 = try allocator.alloc(u32, w + 1);
    checker.checkModal(w, r, v, e, o[0..w]);
    try testing.expectEqual(@intCast(u32, 2), o[0]);
    try testing.expectEqual(@intCast(u32, 0), o[1]);
}

test "necessity1" {
    const w = 2;
    const r: [*:sentinel]const charType = &[_:sentinel]charType{ '(', '1', ',', ' ', '1', ')' };
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ '□', 'p' };
    var o: []u32 = try allocator.alloc(u32, w + 1);
    checker.checkModal(w, r, v, e, o[0..w]);
    try testing.expectEqual(@intCast(u32, 1), o[0]);
    try testing.expectEqual(@intCast(u32, 2), o[1]);
    try testing.expectEqual(@intCast(u32, 0), o[2]);
}

test "necessity2" {
    const w = 10;
    const r: [*:sentinel]const charType = &[_:sentinel]charType{ '(', '1', ',', ' ', '2', ')', ' ', '(', '1', ',', ' ', '3', ')', ' ', '(', '1', ',', ' ', '4', ')', ' ', '(', '2', ',', ' ', '5', ')', ' ', '(', '3', ',', ' ', '6', ')', ' ', '(', '4', ',', ' ', '7', ')', ' ', '(', '5', ',', ' ', '8', ')', ' ', '(', '6', ',', ' ', '9', ')', ' ', '(', '7', ',', ' ', '1', '0', ')' };
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '2', ' ', '3', ' ', '4', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ '□', '□', 'p' };
    var o: []u32 = try allocator.alloc(u32, w + 1);
    checker.checkModal(w, r, v, e, o[0..w]);
    try testing.expectEqual(@intCast(u32, 5), o[0]);
    try testing.expectEqual(@intCast(u32, 6), o[1]);
    try testing.expectEqual(@intCast(u32, 7), o[2]);
    try testing.expectEqual(@intCast(u32, 8), o[3]);
    try testing.expectEqual(@intCast(u32, 9), o[4]);
    try testing.expectEqual(@intCast(u32, 10), o[5]);
    try testing.expectEqual(@intCast(u32, 0), o[6]);
}

test "necessity3" {
    const w = 10;
    const r: [*:sentinel]const charType = &[_:sentinel]charType{ '(', '1', ',', ' ', '2', ')', ' ', '(', '1', ',', ' ', '3', ')', ' ', '(', '1', ',', ' ', '4', ')', ' ', '(', '2', ',', ' ', '5', ')', ' ', '(', '3', ',', ' ', '6', ')', ' ', '(', '4', ',', ' ', '7', ')', ' ', '(', '5', ',', ' ', '8', ')', ' ', '(', '6', ',', ' ', '9', ')', ' ', '(', '7', ',', ' ', '1', '0', ')' };
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '5', ' ', '6', ' ', '7', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ '□', '□', 'p' };
    var o: []u32 = try allocator.alloc(u32, w + 1);
    checker.checkModal(w, r, v, e, o[0..w]);
    try testing.expectEqual(@intCast(u32, 1), o[0]);
    try testing.expectEqual(@intCast(u32, 5), o[1]);
    try testing.expectEqual(@intCast(u32, 6), o[2]);
    try testing.expectEqual(@intCast(u32, 7), o[3]);
    try testing.expectEqual(@intCast(u32, 8), o[4]);
    try testing.expectEqual(@intCast(u32, 9), o[5]);
    try testing.expectEqual(@intCast(u32, 10), o[6]);
    try testing.expectEqual(@intCast(u32, 0), o[7]);
}
