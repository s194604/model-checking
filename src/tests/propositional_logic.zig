const std = @import("std");
const testing = std.testing;
const checker = @import("../checker.zig");
const sentinel = @import("../constants.zig").sentinel;
const charType = @import("../constants.zig").charType;
const allocator = std.heap.page_allocator;

test "verum0" {
    const w = 2;
    const r: [*:sentinel]const charType = &[_:sentinel]charType{};
    const v: [*:sentinel]const charType = &[_:sentinel]charType{};
    const e: [*:sentinel]const charType = &[_:sentinel]charType{'⊤'};
    var o: []u32 = try allocator.alloc(u32, w + 1);
    checker.checkPropositional(w, r, v, e, o[0..w]);
    try testing.expectEqual(@intCast(u32, 1), o[0]);
    try testing.expectEqual(@intCast(u32, 2), o[1]);
    try testing.expectEqual(@intCast(u32, 0), o[2]);
}

test "falsum0" {
    const w = 2;
    const r: [*:sentinel]const charType = &[_:sentinel]charType{};
    const v: [*:sentinel]const charType = &[_:sentinel]charType{};
    const e: [*:sentinel]const charType = &[_:sentinel]charType{'⊥'};
    var o: []u32 = try allocator.alloc(u32, w + 1);
    checker.checkPropositional(w, r, v, e, o[0..w]);
    try testing.expectEqual(@intCast(u32, 0), o[0]);
}

test "negation0" {
    const w = 2;
    const r: [*:sentinel]const charType = &[_:sentinel]charType{};
    const v: [*:sentinel]const charType = &[_:sentinel]charType{};
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ '¬', 'p' };
    var o: []u32 = try allocator.alloc(u32, w + 1);
    checker.checkPropositional(w, r, v, e, o[0..w]);
    try testing.expectEqual(@intCast(u32, 1), o[0]);
    try testing.expectEqual(@intCast(u32, 2), o[1]);
    try testing.expectEqual(@intCast(u32, 0), o[2]);
}

test "negation1" {
    const w = 4;
    const r: [*:sentinel]const charType = &[_:sentinel]charType{};
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', '1', ',', '2', ',', '3', ',', '4' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ '¬', 'p' };
    var o: []u32 = try allocator.alloc(u32, w + 1);
    checker.checkPropositional(w, r, v, e, o[0..w]);
    try testing.expectEqual(@intCast(u32, 0), o[0]);
}

test "conjunction0" {
    const w = 4;
    const r: [*:sentinel]const charType = &[_:sentinel]charType{};
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', ' ', ',', '2', '.', ' ', 'q', ':', ' ', '1', ' ', '2', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', '∧', 'q' };
    var o: []u32 = try allocator.alloc(u32, w + 1);
    checker.checkPropositional(w, r, v, e, o[0..w]);
    try testing.expectEqual(@intCast(u32, 1), o[0]);
    try testing.expectEqual(@intCast(u32, 2), o[1]);
    try testing.expectEqual(@intCast(u32, 0), o[2]);
}

test "conjunction1" {
    const w = 4;
    const r: [*:sentinel]const charType = &[_:sentinel]charType{};
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', ' ', ',', '2', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', '∧', 'q' };
    var o: []u32 = try allocator.alloc(u32, w + 1);
    checker.checkPropositional(w, r, v, e, o[0..w]);
    try testing.expectEqual(@intCast(u32, 0), o[0]);
}

test "conjunction2" {
    const w = 4;
    const r: [*:sentinel]const charType = &[_:sentinel]charType{};
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'q', ':', ' ', '1', ' ', ',', '2', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', '∧', 'q' };
    var o: []u32 = try allocator.alloc(u32, w + 1);
    checker.checkPropositional(w, r, v, e, o[0..w]);
    try testing.expectEqual(@intCast(u32, 0), o[0]);
}

test "disjunction0" {
    const w = 4;
    const r: [*:sentinel]const charType = &[_:sentinel]charType{};
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', ' ', ',', '2', '.', ' ', 'q', ':', ' ', '1', ' ', '2', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', '∨', 'q' };
    var o: []u32 = try allocator.alloc(u32, w + 1);
    checker.checkPropositional(w, r, v, e, o[0..w]);
    try testing.expectEqual(@intCast(u32, 1), o[0]);
    try testing.expectEqual(@intCast(u32, 2), o[1]);
    try testing.expectEqual(@intCast(u32, 0), o[2]);
}

test "disjunction1" {
    const w = 4;
    const r: [*:sentinel]const charType = &[_:sentinel]charType{};
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', ' ', ',', '2', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', '∨', 'q' };
    var o: []u32 = try allocator.alloc(u32, w + 1);
    checker.checkPropositional(w, r, v, e, o[0..w]);
    try testing.expectEqual(@intCast(u32, 1), o[0]);
    try testing.expectEqual(@intCast(u32, 2), o[1]);
    try testing.expectEqual(@intCast(u32, 0), o[2]);
}

test "disjunction2" {
    const w = 4;
    const r: [*:sentinel]const charType = &[_:sentinel]charType{};
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'q', ':', ' ', '1', ' ', ',', '2', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', '∨', 'q' };
    var o: []u32 = try allocator.alloc(u32, w + 1);
    checker.checkPropositional(w, r, v, e, o[0..w]);
    try testing.expectEqual(@intCast(u32, 1), o[0]);
    try testing.expectEqual(@intCast(u32, 2), o[1]);
    try testing.expectEqual(@intCast(u32, 0), o[2]);
}

test "implication0" {
    const w = 4;
    const r: [*:sentinel]const charType = &[_:sentinel]charType{};
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', ' ', ',', '2', '.', ' ', 'q', ':', ' ', '1', ' ', '2', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', '→', 'q' };
    var o: []u32 = try allocator.alloc(u32, w + 1);
    checker.checkPropositional(w, r, v, e, o[0..w]);
    try testing.expectEqual(@intCast(u32, 1), o[0]);
    try testing.expectEqual(@intCast(u32, 2), o[1]);
    try testing.expectEqual(@intCast(u32, 3), o[2]);
    try testing.expectEqual(@intCast(u32, 4), o[3]);
    try testing.expectEqual(@intCast(u32, 0), o[4]);
}

test "implication1" {
    const w = 4;
    const r: [*:sentinel]const charType = &[_:sentinel]charType{};
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', ' ', ',', '2', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', '→', 'q' };
    var o: []u32 = try allocator.alloc(u32, w + 1);
    checker.checkPropositional(w, r, v, e, o[0..w]);
    try testing.expectEqual(@intCast(u32, 3), o[0]);
    try testing.expectEqual(@intCast(u32, 4), o[1]);
    try testing.expectEqual(@intCast(u32, 0), o[2]);
}

test "implication2" {
    const w = 4;
    const r: [*:sentinel]const charType = &[_:sentinel]charType{};
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'q', ':', ' ', '1', ' ', '2', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', '→', 'q' };
    var o: []u32 = try allocator.alloc(u32, w + 1);
    checker.checkPropositional(w, r, v, e, o[0..w]);
    try testing.expectEqual(@intCast(u32, 1), o[0]);
    try testing.expectEqual(@intCast(u32, 2), o[1]);
    try testing.expectEqual(@intCast(u32, 3), o[2]);
    try testing.expectEqual(@intCast(u32, 4), o[3]);
    try testing.expectEqual(@intCast(u32, 0), o[4]);
}

test "biimplication0" {
    const w = 4;
    const r: [*:sentinel]const charType = &[_:sentinel]charType{};
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', ' ', ',', '2', '.', ' ', 'q', ':', ' ', '1', ' ', '2', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', '↔', 'q' };
    var o: []u32 = try allocator.alloc(u32, w + 1);
    checker.checkPropositional(w, r, v, e, o[0..w]);
    try testing.expectEqual(@intCast(u32, 1), o[0]);
    try testing.expectEqual(@intCast(u32, 2), o[1]);
    try testing.expectEqual(@intCast(u32, 3), o[2]);
    try testing.expectEqual(@intCast(u32, 4), o[3]);
    try testing.expectEqual(@intCast(u32, 0), o[4]);
}

test "biimplication1" {
    const w = 4;
    const r: [*:sentinel]const charType = &[_:sentinel]charType{};
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', ' ', ',', '2', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', '↔', 'q' };
    var o: []u32 = try allocator.alloc(u32, w + 1);
    checker.checkPropositional(w, r, v, e, o[0..w]);
    try testing.expectEqual(@intCast(u32, 3), o[0]);
    try testing.expectEqual(@intCast(u32, 4), o[1]);
    try testing.expectEqual(@intCast(u32, 0), o[2]);
}

test "biimplication2" {
    const w = 4;
    const r: [*:sentinel]const charType = &[_:sentinel]charType{};
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'q', ':', ' ', '1', ' ', ',', '2', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', '↔', 'q' };
    var o: []u32 = try allocator.alloc(u32, w + 1);
    checker.checkPropositional(w, r, v, e, o[0..w]);
    try testing.expectEqual(@intCast(u32, 3), o[0]);
    try testing.expectEqual(@intCast(u32, 4), o[1]);
    try testing.expectEqual(@intCast(u32, 0), o[2]);
}

test "multi0" {
    const w = 4;
    const r: [*:sentinel]const charType = &[_:sentinel]charType{};
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', 'r', 'o', 'p', ':', ' ', '1', ' ', ',', '2', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', 'r', 'o', 'p' };
    var o: []u32 = try allocator.alloc(u32, w + 1);
    checker.checkPropositional(w, r, v, e, o[0..w]);
    try testing.expectEqual(@intCast(u32, 1), o[0]);
    try testing.expectEqual(@intCast(u32, 2), o[1]);
    try testing.expectEqual(@intCast(u32, 0), o[2]);
}

test "multi1" {
    const w = 4;
    const r: [*:sentinel]const charType = &[_:sentinel]charType{};
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', ' ', ',', '2', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', 'r', 'o', 'p' };
    var o: []u32 = try allocator.alloc(u32, w + 1);
    checker.checkPropositional(w, r, v, e, o[0..w]);
    try testing.expectEqual(@intCast(u32, 0), o[0]);
}

test "multi2" {
    const w = 4;
    const r: [*:sentinel]const charType = &[_:sentinel]charType{};
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', 'r', 'o', 'p', ':', ' ', '1', ' ', ',', '2', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', 'r', 'o', 'p', '∧', 'p', 'r', 'o', 'p' };
    var o: []u32 = try allocator.alloc(u32, w + 1);
    checker.checkPropositional(w, r, v, e, o[0..w]);
    try testing.expectEqual(@intCast(u32, 1), o[0]);
    try testing.expectEqual(@intCast(u32, 2), o[1]);
    try testing.expectEqual(@intCast(u32, 0), o[2]);
}

test "multi3" {
    const w = 4;
    const r: [*:sentinel]const charType = &[_:sentinel]charType{};
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', 'r', 'o', 'p', ':', ' ', '1', ' ', ',', '2', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', 'r', 'o', 'p', '∧', 'p', 'r', 'o', 'q' };
    var o: []u32 = try allocator.alloc(u32, w + 1);
    checker.checkPropositional(w, r, v, e, o[0..w]);
    try testing.expectEqual(@intCast(u32, 0), o[0]);
}

test "multi4" {
    const w = 4;
    const r: [*:sentinel]const charType = &[_:sentinel]charType{};
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', 'r', 'o', 'p', ':', ' ', '1', ' ', ',', '2', ' ', 'p', 'r', 'o', 'q', ':', ' ', '1', ' ', ',', '2', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', 'r', 'o', 'p', '∧', 'p', 'r', 'o', 'q' };
    var o: []u32 = try allocator.alloc(u32, w + 1);
    checker.checkPropositional(w, r, v, e, o[0..w]);
    try testing.expectEqual(@intCast(u32, 1), o[0]);
    try testing.expectEqual(@intCast(u32, 2), o[1]);
    try testing.expectEqual(@intCast(u32, 0), o[2]);
}
