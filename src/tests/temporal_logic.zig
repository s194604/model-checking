const std = @import("std");
const testing = std.testing;
const typeData = @import("../types.zig");
const checker = @import("../checker.zig");
const sentinel = @import("../constants.zig").sentinel;
const charType = @import("../constants.zig").charType;
const infinity = @import("../constants.zig").infinity;
const allocator = std.heap.page_allocator;

test "verum0" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{};
    const e: [*:sentinel]const charType = &[_:sentinel]charType{'⊤'};
    const expectedA = [_]u32{1};
    const expectedB = [_]u32{0};
    const expectedLength: u32 = 1;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    const actualA = actual[0];
    const actualB = actual[1];
    try testing.expectEqual(expectedLength, actualLength);
    try testing.expectEqual(expectedA[0], actualA[0]);
    try testing.expectEqual(expectedB[0], actualB[0]);
}

test "falsum0" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{};
    const e: [*:sentinel]const charType = &[_:sentinel]charType{'⊥'};
    const expectedLength: u32 = 0;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    try testing.expectEqual(expectedLength, actualLength);
}

test "negation0" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{};
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ '¬', 'p' };
    const expectedA = [_]u32{1};
    const expectedB = [_]u32{0};
    const expectedLength: u32 = 1;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    const actualA = actual[0];
    const actualB = actual[1];
    try testing.expectEqual(expectedLength, actualLength);
    try testing.expectEqual(expectedA[0], actualA[0]);
    try testing.expectEqual(expectedB[0], actualB[0]);
}

test "negation1" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', 'A', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ '¬', 'p' };
    const expectedLength: u32 = 0;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    try testing.expectEqual(expectedLength, actualLength);
}

test "negation2" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '3', ',', '5', 'U', '4', '2', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ '¬', 'p' };
    const expectedA = [_]u32{ 1, 4, 43 };
    const expectedB = [_]u32{ 2, 4, 0 };
    const expectedLength: u32 = 3;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    const actualA = actual[0];
    const actualB = actual[1];
    try testing.expectEqual(expectedLength, actualLength);
    try testing.expectEqual(expectedA[0], actualA[0]);
    try testing.expectEqual(expectedB[0], actualB[0]);
    try testing.expectEqual(expectedA[1], actualA[1]);
    try testing.expectEqual(expectedB[1], actualB[1]);
    try testing.expectEqual(expectedA[2], actualA[2]);
    try testing.expectEqual(expectedB[2], actualB[2]);
}

test "negation3" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', 'U', '3', ',', '6', 'U', '7', ',', '9', 'A', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ '¬', 'p' };
    const expectedA = [_]u32{ 4, 8 };
    const expectedB = [_]u32{ 5, 8 };
    const expectedLength: u32 = 2;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    const actualA = actual[0];
    const actualB = actual[1];
    try testing.expectEqual(expectedLength, actualLength);
    try testing.expectEqual(expectedA[0], actualA[0]);
    try testing.expectEqual(expectedB[0], actualB[0]);
    try testing.expectEqual(expectedA[1], actualA[1]);
    try testing.expectEqual(expectedB[1], actualB[1]);
}

test "conjunction0" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{};
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', '∧', 'q' };
    const expectedLength: u32 = 0;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    try testing.expectEqual(expectedLength, actualLength);
}

test "conjunction1" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', 'A', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', '∧', 'q' };
    const expectedLength: u32 = 0;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    try testing.expectEqual(expectedLength, actualLength);
}

test "conjunction2" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'q', ':', ' ', '1', 'A', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', '∧', 'q' };
    const expectedLength: u32 = 0;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    try testing.expectEqual(expectedLength, actualLength);
}

test "conjunction3" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', 'A', '.', ' ', 'q', ':', ' ', '1', 'A', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', '∧', 'q' };
    const expectedA = [_]u32{1};
    const expectedB = [_]u32{0};
    const expectedLength: u32 = 1;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    const actualA = actual[0];
    const actualB = actual[1];
    try testing.expectEqual(expectedLength, actualLength);
    try testing.expectEqual(expectedA[0], actualA[0]);
    try testing.expectEqual(expectedB[0], actualB[0]);
}

test "conjunction4" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', 'U', '5', ',', '9', 'U', '4', '2', '.', ' ', 'q', ':', ' ', '3', 'U', '6', ',', '3', '0', 'A', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', '∧', 'q' };
    const expectedA = [_]u32{ 3, 30 };
    const expectedB = [_]u32{ 5, 42 };
    const expectedLength: u32 = 2;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    const actualA = actual[0];
    const actualB = actual[1];
    try testing.expectEqual(expectedLength, actualLength);
    try testing.expectEqual(expectedA[0], actualA[0]);
    try testing.expectEqual(expectedB[0], actualB[0]);
    try testing.expectEqual(expectedA[1], actualA[1]);
    try testing.expectEqual(expectedB[1], actualB[1]);
}

test "disjunction0" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{};
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', '∨', 'q' };
    const expectedLength: u32 = 0;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    try testing.expectEqual(expectedLength, actualLength);
}

test "disjunction1" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', 'A', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', '∨', 'q' };
    const expectedA = [_]u32{1};
    const expectedB = [_]u32{0};
    const expectedLength: u32 = 1;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    const actualA = actual[0];
    const actualB = actual[1];
    try testing.expectEqual(expectedLength, actualLength);
    try testing.expectEqual(expectedA[0], actualA[0]);
    try testing.expectEqual(expectedB[0], actualB[0]);
}

test "disjunction2" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', 'A', '.', ' ', 'q', ':', ' ', '1', 'A', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', '∨', 'q' };
    const expectedA = [_]u32{1};
    const expectedB = [_]u32{0};
    const expectedLength: u32 = 1;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    const actualA = actual[0];
    const actualB = actual[1];
    try testing.expectEqual(expectedLength, actualLength);
    try testing.expectEqual(expectedA[0], actualA[0]);
    try testing.expectEqual(expectedB[0], actualB[0]);
}

test "disjunction3" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', 'U', '5', ',', '9', 'U', '4', '2', '.', ' ', 'q', ':', ' ', '3', 'U', '6', ',', '3', '0', 'A', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', '∨', 'q' };
    const expectedA = [_]u32{ 1, 9 };
    const expectedB = [_]u32{ 6, 0 };
    const expectedLength: u32 = 2;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    const actualA = actual[0];
    const actualB = actual[1];
    try testing.expectEqual(expectedLength, actualLength);
    try testing.expectEqual(expectedA[0], actualA[0]);
    try testing.expectEqual(expectedB[0], actualB[0]);
    try testing.expectEqual(expectedA[1], actualA[1]);
    try testing.expectEqual(expectedB[1], actualB[1]);
}

test "implication0" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{};
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', '→', 'q' };
    const expectedA = [_]u32{1};
    const expectedB = [_]u32{0};
    const expectedLength: u32 = 1;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    const actualA = actual[0];
    const actualB = actual[1];
    try testing.expectEqual(expectedLength, actualLength);
    try testing.expectEqual(expectedA[0], actualA[0]);
    try testing.expectEqual(expectedB[0], actualB[0]);
}

test "implication1" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', 'A', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', '→', 'q' };
    const expectedLength: u32 = 0;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    try testing.expectEqual(expectedLength, actualLength);
}

test "implication2" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', 'A', '.', ' ', 'q', ':', ' ', '1', 'A', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', '→', 'q' };
    const expectedA = [_]u32{1};
    const expectedB = [_]u32{0};
    const expectedLength: u32 = 1;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    const actualA = actual[0];
    const actualB = actual[1];
    try testing.expectEqual(expectedLength, actualLength);
    try testing.expectEqual(expectedA[0], actualA[0]);
    try testing.expectEqual(expectedB[0], actualB[0]);
}

test "implication3" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', 'U', '5', ',', '9', 'U', '4', '2', '.', ' ', 'q', ':', ' ', '3', 'U', '6', ',', '3', '0', 'A', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', '→', 'q' };
    const expectedA = [_]u32{ 3, 30 };
    const expectedB = [_]u32{ 8, 0 };
    const expectedLength: u32 = 2;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    const actualA = actual[0];
    const actualB = actual[1];
    try testing.expectEqual(expectedLength, actualLength);
    try testing.expectEqual(expectedA[0], actualA[0]);
    try testing.expectEqual(expectedB[0], actualB[0]);
    try testing.expectEqual(expectedA[1], actualA[1]);
    try testing.expectEqual(expectedB[1], actualB[1]);
}

test "biimplication0" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{};
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', '↔', 'q' };
    const expectedA = [_]u32{1};
    const expectedB = [_]u32{0};
    const expectedLength: u32 = 1;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    const actualA = actual[0];
    const actualB = actual[1];
    try testing.expectEqual(expectedLength, actualLength);
    try testing.expectEqual(expectedA[0], actualA[0]);
    try testing.expectEqual(expectedB[0], actualB[0]);
}

test "biimplication1" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', 'A', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', '↔', 'q' };
    const expectedLength: u32 = 0;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    try testing.expectEqual(expectedLength, actualLength);
}

test "biimplication2" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', 'A', '.', ' ', 'q', ':', ' ', '1', 'A', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', '↔', 'q' };
    const expectedA = [_]u32{1};
    const expectedB = [_]u32{0};
    const expectedLength: u32 = 1;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    const actualA = actual[0];
    const actualB = actual[1];
    try testing.expectEqual(expectedLength, actualLength);
    try testing.expectEqual(expectedA[0], actualA[0]);
    try testing.expectEqual(expectedB[0], actualB[0]);
}

test "biimplication3" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', 'U', '5', ',', '9', 'U', '4', '2', '.', ' ', 'q', ':', ' ', '3', 'U', '6', ',', '3', '0', 'A', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', '↔', 'q' };
    const expectedA = [_]u32{ 3, 7, 30 };
    const expectedB = [_]u32{ 5, 8, 42 };
    const expectedLength: u32 = 3;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    const actualA = actual[0];
    const actualB = actual[1];
    try testing.expectEqual(expectedLength, actualLength);
    try testing.expectEqual(expectedA[0], actualA[0]);
    try testing.expectEqual(expectedB[0], actualB[0]);
    try testing.expectEqual(expectedA[1], actualA[1]);
    try testing.expectEqual(expectedB[1], actualB[1]);
    try testing.expectEqual(expectedA[2], actualA[2]);
    try testing.expectEqual(expectedB[2], actualB[2]);
}

test "next0" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{};
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'X', 'p' };
    const expectedLength: u32 = 0;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    try testing.expectEqual(expectedLength, actualLength);
}

test "next1" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', 'A', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'X', 'p' };
    const expectedA = [_]u32{1};
    const expectedB = [_]u32{0};
    const expectedLength: u32 = 1;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    const actualA = actual[0];
    const actualB = actual[1];
    try testing.expectEqual(expectedLength, actualLength);
    try testing.expectEqual(expectedA[0], actualA[0]);
    try testing.expectEqual(expectedB[0], actualB[0]);
}

test "next2" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', 'U', '3', ',', '5', 'U', '7', ',', '9', 'A', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'X', 'X', 'p' };
    const expectedA = [_]u32{ 1, 3, 7 };
    const expectedB = [_]u32{ 1, 5, 0 };
    const expectedLength: u32 = 3;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    const actualA = actual[0];
    const actualB = actual[1];
    try testing.expectEqual(expectedLength, actualLength);
    try testing.expectEqual(expectedA[0], actualA[0]);
    try testing.expectEqual(expectedB[0], actualB[0]);
    try testing.expectEqual(expectedA[1], actualA[1]);
    try testing.expectEqual(expectedB[1], actualB[1]);
    try testing.expectEqual(expectedA[2], actualA[2]);
    try testing.expectEqual(expectedB[2], actualB[2]);
}

test "future0" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{};
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'F', 'p' };
    const expectedLength: u32 = 0;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    try testing.expectEqual(expectedLength, actualLength);
}

test "future1" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', 'A', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'F', 'p' };
    const expectedA = [_]u32{1};
    const expectedB = [_]u32{0};
    const expectedLength: u32 = 1;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    const actualA = actual[0];
    const actualB = actual[1];
    try testing.expectEqual(expectedLength, actualLength);
    try testing.expectEqual(expectedA[0], actualA[0]);
    try testing.expectEqual(expectedB[0], actualB[0]);
}

test "future2" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', 'U', '3', ',', '5', 'U', '7', ',', '9', 'A', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'F', 'p' };
    const expectedA = [_]u32{1};
    const expectedB = [_]u32{0};
    const expectedLength: u32 = 1;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    const actualA = actual[0];
    const actualB = actual[1];
    try testing.expectEqual(expectedLength, actualLength);
    try testing.expectEqual(expectedA[0], actualA[0]);
    try testing.expectEqual(expectedB[0], actualB[0]);
}

test "future3" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', 'U', '3', ',', '5', 'U', '7', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'F', 'p' };
    const expectedA = [_]u32{1};
    const expectedB = [_]u32{7};
    const expectedLength: u32 = 1;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    const actualA = actual[0];
    const actualB = actual[1];
    try testing.expectEqual(expectedLength, actualLength);
    try testing.expectEqual(expectedA[0], actualA[0]);
    try testing.expectEqual(expectedB[0], actualB[0]);
}

test "global0" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{};
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'G', 'p' };
    const expectedLength: u32 = 0;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    try testing.expectEqual(expectedLength, actualLength);
}

test "global1" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', 'A', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'G', 'p' };
    const expectedA = [_]u32{1};
    const expectedB = [_]u32{0};
    const expectedLength: u32 = 1;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    const actualA = actual[0];
    const actualB = actual[1];
    try testing.expectEqual(expectedLength, actualLength);
    try testing.expectEqual(expectedA[0], actualA[0]);
    try testing.expectEqual(expectedB[0], actualB[0]);
}

test "global2" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', 'U', '3', ',', '5', 'U', '7', ',', '9', 'A', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'G', 'p' };
    const expectedA = [_]u32{9};
    const expectedB = [_]u32{0};
    const expectedLength: u32 = 1;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    const actualA = actual[0];
    const actualB = actual[1];
    try testing.expectEqual(expectedLength, actualLength);
    try testing.expectEqual(expectedA[0], actualA[0]);
    try testing.expectEqual(expectedB[0], actualB[0]);
}

test "until0" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{};
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ' ', 'U', ' ', 'q' };
    const expectedLength: u32 = 0;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    try testing.expectEqual(expectedLength, actualLength);
}

test "until1" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', 'A', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ' ', 'U', ' ', 'q' };
    const expectedLength: u32 = 0;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    try testing.expectEqual(expectedLength, actualLength);
}

test "until2" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'q', ':', ' ', '1', 'A', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ' ', 'U', ' ', 'q' };
    const expectedA = [_]u32{1};
    const expectedB = [_]u32{0};
    const expectedLength: u32 = 1;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    const actualA = actual[0];
    const actualB = actual[1];
    try testing.expectEqual(expectedLength, actualLength);
    try testing.expectEqual(expectedA[0], actualA[0]);
    try testing.expectEqual(expectedB[0], actualB[0]);
}

test "until3" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', 'A', ';', ' ', 'q', ':', ' ', '1', 'A', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ' ', 'U', ' ', 'q' };
    const expectedA = [_]u32{1};
    const expectedB = [_]u32{0};
    const expectedLength: u32 = 1;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    const actualA = actual[0];
    const actualB = actual[1];
    try testing.expectEqual(expectedLength, actualLength);
    try testing.expectEqual(expectedA[0], actualA[0]);
    try testing.expectEqual(expectedB[0], actualB[0]);
}

test "until4" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', 'U', '5', ',', '9', 'U', '4', '2', '.', ' ', 'q', ':', ' ', '3', 'U', '6', ',', '3', '0', 'A', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ' ', 'U', ' ', 'q' };
    const expectedA = [_]u32{ 1, 9 };
    const expectedB = [_]u32{ 6, 0 };
    const expectedLength: u32 = 2;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    const actualA = actual[0];
    const actualB = actual[1];
    try testing.expectEqual(expectedLength, actualLength);
    try testing.expectEqual(expectedA[0], actualA[0]);
    try testing.expectEqual(expectedB[0], actualB[0]);
    try testing.expectEqual(expectedA[1], actualA[1]);
    try testing.expectEqual(expectedB[1], actualB[1]);
}

test "release0" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{};
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ' ', 'R', ' ', 'q' };
    const expectedLength: u32 = 0;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    try testing.expectEqual(expectedLength, actualLength);
}

test "release1" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', 'A', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ' ', 'R', ' ', 'q' };
    const expectedLength: u32 = 0;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    try testing.expectEqual(expectedLength, actualLength);
}

test "release2" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'q', ':', ' ', '1', 'A', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ' ', 'R', ' ', 'q' };
    const expectedA = [_]u32{1};
    const expectedB = [_]u32{0};
    const expectedLength: u32 = 1;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    const actualA = actual[0];
    const actualB = actual[1];
    try testing.expectEqual(expectedLength, actualLength);
    try testing.expectEqual(expectedA[0], actualA[0]);
    try testing.expectEqual(expectedB[0], actualB[0]);
}

test "release3" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', 'A', ';', ' ', 'q', ':', ' ', '1', 'A', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ' ', 'R', ' ', 'q' };
    const expectedA = [_]u32{1};
    const expectedB = [_]u32{0};
    const expectedLength: u32 = 1;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    const actualA = actual[0];
    const actualB = actual[1];
    try testing.expectEqual(expectedLength, actualLength);
    try testing.expectEqual(expectedA[0], actualA[0]);
    try testing.expectEqual(expectedB[0], actualB[0]);
}

test "release4" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', 'U', '5', ',', '9', 'U', '4', '2', '.', ' ', 'q', ':', ' ', '3', 'U', '6', ',', '3', '0', 'A', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ' ', 'R', ' ', 'q' };
    const expectedA = [_]u32{ 3, 30 };
    const expectedB = [_]u32{ 5, 0 };
    const expectedLength: u32 = 2;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    const actualA = actual[0];
    const actualB = actual[1];
    try testing.expectEqual(expectedLength, actualLength);
    try testing.expectEqual(expectedA[0], actualA[0]);
    try testing.expectEqual(expectedB[0], actualB[0]);
    try testing.expectEqual(expectedA[1], actualA[1]);
    try testing.expectEqual(expectedB[1], actualB[1]);
}

test "weak until0" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{};
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ' ', 'W', ' ', 'q' };
    const expectedLength: u32 = 0;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    try testing.expectEqual(expectedLength, actualLength);
}

test "weak until1" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', 'A', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ' ', 'W', ' ', 'q' };
    const expectedA = [_]u32{1};
    const expectedB = [_]u32{0};
    const expectedLength: u32 = 1;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    const actualA = actual[0];
    const actualB = actual[1];
    try testing.expectEqual(expectedLength, actualLength);
    try testing.expectEqual(expectedA[0], actualA[0]);
    try testing.expectEqual(expectedB[0], actualB[0]);
}

test "weak until2" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'q', ':', ' ', '1', 'A', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ' ', 'W', ' ', 'q' };
    const expectedA = [_]u32{1};
    const expectedB = [_]u32{0};
    const expectedLength: u32 = 1;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    const actualA = actual[0];
    const actualB = actual[1];
    try testing.expectEqual(expectedLength, actualLength);
    try testing.expectEqual(expectedA[0], actualA[0]);
    try testing.expectEqual(expectedB[0], actualB[0]);
}

test "weak until3" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', 'A', ';', ' ', 'q', ':', ' ', '1', 'A', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ' ', 'W', ' ', 'q' };
    const expectedA = [_]u32{1};
    const expectedB = [_]u32{0};
    const expectedLength: u32 = 1;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    const actualA = actual[0];
    const actualB = actual[1];
    try testing.expectEqual(expectedLength, actualLength);
    try testing.expectEqual(expectedA[0], actualA[0]);
    try testing.expectEqual(expectedB[0], actualB[0]);
}

test "weak until4" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', 'U', '5', ',', '9', 'U', '4', '2', '.', ' ', 'q', ':', ' ', '3', 'U', '6', ',', '3', '0', 'A', '.' };
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ' ', 'W', ' ', 'q' };
    const expectedA = [_]u32{ 1, 9 };
    const expectedB = [_]u32{ 6, 0 };
    const expectedLength: u32 = 2;
    var actual: [][*]u32 = try allocator.alloc([*]u32, 2);
    const actualLength = checker.checkTemporal(v, e, actual[0..1]);
    const actualA = actual[0];
    const actualB = actual[1];
    try testing.expectEqual(expectedLength, actualLength);
    try testing.expectEqual(expectedA[0], actualA[0]);
    try testing.expectEqual(expectedB[0], actualB[0]);
    try testing.expectEqual(expectedA[1], actualA[1]);
    try testing.expectEqual(expectedB[1], actualB[1]);
}
