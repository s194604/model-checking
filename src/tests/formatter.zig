const std = @import("std");
const testing = std.testing;
const typeData = @import("../types.zig");
const formatter = @import("../formatter.zig");
const sentinel = @import("../constants.zig").sentinel;
const charType = @import("../constants.zig").charType;
const infinity = @import("../constants.zig").infinity;
const allocator = std.heap.page_allocator;
const Vector = std.meta.Vector;
const ArrayList = std.ArrayList;
const Queue = std.atomic.Queue;
const HashMap = std.AutoArrayHashMap;

test "relations0" {
    const r: [*:sentinel]const charType = &[_:sentinel]charType{ '(', '1', ',', ' ', '1', ')' };
    const actual = formatter.parseRelations(r);
    var expected = ArrayList(Vector(2, u32)).init(allocator);
    try expected.append(.{ 1, 1 });
    try testing.expectEqual(expected.items.len, actual.*.items.len);
    try checkEqualRelationsLists(&expected, actual);
}

test "relations1" {
    const r: [*:sentinel]const charType = &[_:sentinel]charType{ '(', '1', ',', ' ', '2', ')', ' ', '(', '2', ',', ' ', '3', ')' };
    const actual = formatter.parseRelations(r);
    var expected = ArrayList(Vector(2, u32)).init(allocator);
    try expected.append(.{ 1, 2 });
    try expected.append(.{ 2, 3 });
    try testing.expectEqual(expected.items.len, actual.*.items.len);
    try checkEqualRelationsLists(&expected, actual);
}

test "relations2" {
    const r: [*:sentinel]const charType = &[_:sentinel]charType{ '(', '1', '0', ',', ' ', '1', '1', ')', ' ', '(', '1', '1', ',', ' ', '1', '2', ')' };
    const actual = formatter.parseRelations(r);
    var expected = ArrayList(Vector(2, u32)).init(allocator);
    try expected.append(.{ 10, 11 });
    try expected.append(.{ 11, 12 });
    try testing.expectEqual(expected.items.len, actual.*.items.len);
    try checkEqualRelationsLists(&expected, actual);
}

test "valuations0" {
    const w = 2;
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', ' ', ',', '2', '.' };
    const actual = formatter.parseValuations(w, v);
    var expected = ArrayList(ArrayList(charType)).init(allocator);
    var i: usize = 0;
    while (i < w) : (i += 1) {
        try expected.append(ArrayList(charType).init(allocator));
    }
    try expected.items[1 - 1].append('p');
    try expected.items[2 - 1].append('p');
    try testing.expectEqual(expected.items.len, actual.*.items.len);
    try checkEqualValuationsLists(&expected, actual);
}

test "valuations1" {
    const w = 2;
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', '.', ' ', 'q', ':', ' ', '1', '.' };
    const actual = formatter.parseValuations(w, v);
    var expected = ArrayList(ArrayList(charType)).init(allocator);
    var i: usize = 0;
    while (i < w) : (i += 1) {
        try expected.append(ArrayList(charType).init(allocator));
    }
    try expected.items[1 - 1].append('p');
    try expected.items[1 - 1].append('q');
    try testing.expectEqual(expected.items.len, actual.*.items.len);
    try checkEqualValuationsLists(&expected, actual);
}

test "valuations2" {
    const w = 12;
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', '0', ' ', '1', '1', ' ', '1', '2', '.' };
    const actual = formatter.parseValuations(w, v);
    var expected = ArrayList(ArrayList(charType)).init(allocator);
    var i: usize = 0;
    while (i < w) : (i += 1) {
        try expected.append(ArrayList(charType).init(allocator));
    }
    try expected.items[10 - 1].append('p');
    try expected.items[11 - 1].append('p');
    try expected.items[12 - 1].append('p');
    try testing.expectEqual(expected.items.len, actual.*.items.len);
    try checkEqualValuationsLists(&expected, actual);
}

test "expression0" {
    const e: [*:sentinel]const charType = &[_:sentinel]charType{'p'};
    var tokenContainerActual = ArrayList(Queue(typeData.Token).Node).init(allocator);
    var actual = formatter.tokenizeExpression(e, &tokenContainerActual, typeData.logicalSystem.PROPOSITIONAL);
    const terminal: ?*Queue(typeData.Token).Node = null;
    try testing.expectEqual(typeData.Type.PROPOSITION, actual.get().?.data.type);
    try testing.expectEqual(terminal, actual.get());
}

test "expression1" {
    const e: [*:sentinel]const charType = &[_:sentinel]charType{'¬'};
    var tokenContainerActual = ArrayList(Queue(typeData.Token).Node).init(allocator);
    var actual = formatter.tokenizeExpression(e, &tokenContainerActual, typeData.logicalSystem.PROPOSITIONAL);
    const terminal: ?*Queue(typeData.Token).Node = null;
    try testing.expectEqual(typeData.Type.NOT, actual.get().?.data.type);
    try testing.expectEqual(terminal, actual.get());
}

test "expression2" {
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', '∧', 'q' };
    var tokenContainerActual = ArrayList(Queue(typeData.Token).Node).init(allocator);
    var actual = formatter.tokenizeExpression(e, &tokenContainerActual, typeData.logicalSystem.PROPOSITIONAL);
    const terminal: ?*Queue(typeData.Token).Node = null;
    try testing.expectEqual(typeData.Type.PROPOSITION, actual.get().?.data.type);
    try testing.expectEqual(typeData.Type.AND, actual.get().?.data.type);
    try testing.expectEqual(typeData.Type.PROPOSITION, actual.get().?.data.type);
    try testing.expectEqual(terminal, actual.get());
}

test "expression3" {
    const e: [*:sentinel]const charType = &[_:sentinel]charType{'□'};
    var tokenContainerActual = ArrayList(Queue(typeData.Token).Node).init(allocator);
    var actual = formatter.tokenizeExpression(e, &tokenContainerActual, typeData.logicalSystem.MODAL);
    const terminal: ?*Queue(typeData.Token).Node = null;
    try testing.expectEqual(typeData.Type.MODAL_ALWAYS, actual.get().?.data.type);
    try testing.expectEqual(terminal, actual.get());
}

test "expression4" {
    const e: [*:sentinel]const charType = &[_:sentinel]charType{'X'};
    var tokenContainerActual = ArrayList(Queue(typeData.Token).Node).init(allocator);
    var actual = formatter.tokenizeExpression(e, &tokenContainerActual, typeData.logicalSystem.TEMPORAL);
    const terminal: ?*Queue(typeData.Token).Node = null;
    try testing.expectEqual(typeData.Type.TEMPORAL_NEXT, actual.get().?.data.type);
    try testing.expectEqual(terminal, actual.get());
}

test "expression5" {
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ' ', 'U', ' ', 'q' };
    var tokenContainerActual = ArrayList(Queue(typeData.Token).Node).init(allocator);
    var actual = formatter.tokenizeExpression(e, &tokenContainerActual, typeData.logicalSystem.TEMPORAL);
    const terminal: ?*Queue(typeData.Token).Node = null;
    try testing.expectEqual(typeData.Type.PROPOSITION, actual.get().?.data.type);
    try testing.expectEqual(typeData.Type.TEMPORAL_UNTIL, actual.get().?.data.type);
    try testing.expectEqual(typeData.Type.PROPOSITION, actual.get().?.data.type);
    try testing.expectEqual(terminal, actual.get());
}

test "expression6" {
    const e: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', 'r', 'o', 'p' };
    var tokenContainerActual = ArrayList(Queue(typeData.Token).Node).init(allocator);
    var actual = formatter.tokenizeExpression(e, &tokenContainerActual, typeData.logicalSystem.PROPOSITIONAL);
    const terminal: ?*Queue(typeData.Token).Node = null;
    try testing.expectEqual(typeData.Type.PROPOSITION, actual.get().?.data.type);
    try testing.expectEqual(terminal, actual.get());
}

test "linear0" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', '.' };
    const actual = formatter.parseLinearValuations(v);
    var expected = HashMap(charType, ArrayList(typeData.linearValuation)).init(allocator);
    try expected.put('p', ArrayList(typeData.linearValuation).init(allocator));
    try expected.getPtr('p').?.*.append(typeData.linearValuation{ .from = 1, .until = 1 });
    try testing.expectEqual(expected.count(), actual.*.count());
    const actualArray = actual.*.get('p');
    const expectedArray = expected.get('p');
    try testing.expectEqual(expectedArray.?.items.len, actualArray.?.items.len);
    try testing.expectEqual(expectedArray.?.items[0].from, actualArray.?.items[0].from);
    try testing.expectEqual(expectedArray.?.items[0].until, actualArray.?.items[0].until);
}

test "linear1" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', ',', ' ', '2', '.' };
    const actual = formatter.parseLinearValuations(v);
    var expected = HashMap(charType, ArrayList(typeData.linearValuation)).init(allocator);
    try expected.put('p', ArrayList(typeData.linearValuation).init(allocator));
    try expected.getPtr('p').?.*.append(typeData.linearValuation{ .from = 1, .until = 2 });
    try testing.expectEqual(expected.count(), actual.*.count());
    const actualArray = actual.*.get('p');
    const expectedArray = expected.get('p');
    try testing.expectEqual(expectedArray.?.items.len, actualArray.?.items.len);
    try testing.expectEqual(expectedArray.?.items[0].from, actualArray.?.items[0].from);
    try testing.expectEqual(expectedArray.?.items[0].until, actualArray.?.items[0].until);
}

test "linear2" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '4', ',', ' ', '1', ',', ' ', '2', ',', ' ', '5', ',', ' ', '6', ',', ' ', '7', ',', ' ', '.' };
    const actual = formatter.parseLinearValuations(v);
    var expected = HashMap(charType, ArrayList(typeData.linearValuation)).init(allocator);
    try expected.put('p', ArrayList(typeData.linearValuation).init(allocator));
    try expected.getPtr('p').?.*.append(typeData.linearValuation{ .from = 1, .until = 2 });
    try expected.getPtr('p').?.*.append(typeData.linearValuation{ .from = 4, .until = 7 });
    try testing.expectEqual(expected.count(), actual.*.count());
    const actualArray = actual.*.get('p');
    const expectedArray = expected.get('p');
    try testing.expectEqual(expectedArray.?.items.len, actualArray.?.items.len);
    try testing.expectEqual(expectedArray.?.items[0], actualArray.?.items[0]);
    try testing.expectEqual(expectedArray.?.items[1], actualArray.?.items[1]);
}

test "linear3" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '4', ',', ' ', '1', 'U', '2', ',', ' ', '5', 'U', '7', '.' };
    const actual = formatter.parseLinearValuations(v);
    var expected = HashMap(charType, ArrayList(typeData.linearValuation)).init(allocator);
    try expected.put('p', ArrayList(typeData.linearValuation).init(allocator));
    try expected.getPtr('p').?.*.append(typeData.linearValuation{ .from = 1, .until = 2 });
    try expected.getPtr('p').?.*.append(typeData.linearValuation{ .from = 4, .until = 7 });
    try testing.expectEqual(expected.count(), actual.*.count());
    const actualArray = actual.*.get('p');
    const expectedArray = expected.get('p');
    try testing.expectEqual(expectedArray.?.items.len, actualArray.?.items.len);
    try testing.expectEqual(expectedArray.?.items[0], actualArray.?.items[0]);
    try testing.expectEqual(expectedArray.?.items[1], actualArray.?.items[1]);
}

test "linear4" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', 'A', '.' };
    const actual = formatter.parseLinearValuations(v);
    var expected = HashMap(charType, ArrayList(typeData.linearValuation)).init(allocator);
    try expected.put('p', ArrayList(typeData.linearValuation).init(allocator));
    try expected.getPtr('p').?.*.append(typeData.linearValuation{ .from = 1, .until = 0 });
    try testing.expectEqual(expected.count(), actual.*.count());
    const actualArray = actual.*.get('p');
    const expectedArray = expected.get('p');
    try testing.expectEqual(expectedArray.?.items.len, actualArray.?.items.len);
    try testing.expectEqual(expectedArray.?.items[0], actualArray.?.items[0]);
}

test "linear5" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', 'A', ',', ' ', '2', 'A', '.' };
    const actual = formatter.parseLinearValuations(v);
    var expected = HashMap(charType, ArrayList(typeData.linearValuation)).init(allocator);
    try expected.put('p', ArrayList(typeData.linearValuation).init(allocator));
    try expected.getPtr('p').?.*.append(typeData.linearValuation{ .from = 1, .until = 0 });
    try testing.expectEqual(expected.count(), actual.*.count());
    const actualArray = actual.*.get('p');
    const expectedArray = expected.get('p');
    try testing.expectEqual(expectedArray.?.items.len, actualArray.?.items.len);
    try testing.expectEqual(expectedArray.?.items[0], actualArray.?.items[0]);
}

test "linear6" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '4', '2', ',', ' ', '1', 'U', '2', ',', ' ', '7', 'A', ',', ' ', '5', 'A', '.' };
    const actual = formatter.parseLinearValuations(v);
    var expected = HashMap(charType, ArrayList(typeData.linearValuation)).init(allocator);
    try expected.put('p', ArrayList(typeData.linearValuation).init(allocator));
    try expected.getPtr('p').?.*.append(typeData.linearValuation{ .from = 1, .until = 2 });
    try expected.getPtr('p').?.*.append(typeData.linearValuation{ .from = 5, .until = 0 });
    try testing.expectEqual(expected.count(), actual.*.count());
    const actualArray = actual.*.get('p');
    const expectedArray = expected.get('p');
    try testing.expectEqual(expectedArray.?.items.len, actualArray.?.items.len);
    try testing.expectEqual(expectedArray.?.items[0], actualArray.?.items[0]);
    try testing.expectEqual(expectedArray.?.items[1], actualArray.?.items[1]);
}

test "linear7" {
    const v: [*:sentinel]const charType = &[_:sentinel]charType{ 'p', ':', ' ', '1', 'U', '5', ',', '9', 'U', '4', '2', '.', ' ', 'q', ':', ' ', '3', 'U', '6', ',', '3', '0', 'A', '.' };
    const actual = formatter.parseLinearValuations(v);
    var expected = HashMap(charType, ArrayList(typeData.linearValuation)).init(allocator);
    try expected.put('p', ArrayList(typeData.linearValuation).init(allocator));
    try expected.getPtr('p').?.*.append(typeData.linearValuation{ .from = 1, .until = 5 });
    try expected.getPtr('p').?.*.append(typeData.linearValuation{ .from = 9, .until = 42 });
    try expected.put('q', ArrayList(typeData.linearValuation).init(allocator));
    try expected.getPtr('q').?.*.append(typeData.linearValuation{ .from = 3, .until = 6 });
    try expected.getPtr('q').?.*.append(typeData.linearValuation{ .from = 30, .until = 0 });
    try testing.expectEqual(expected.count(), actual.*.count());
    const actualArrayP = actual.*.get('p');
    const expectedArrayP = expected.get('p');
    const actualArrayQ = actual.*.get('q');
    const expectedArrayQ = expected.get('q');
    try testing.expectEqual(expectedArrayP.?.items.len, actualArrayP.?.items.len);
    try testing.expectEqual(expectedArrayP.?.items[0], actualArrayP.?.items[0]);
    try testing.expectEqual(expectedArrayP.?.items[1], actualArrayP.?.items[1]);
    try testing.expectEqual(expectedArrayQ.?.items.len, actualArrayQ.?.items.len);
    try testing.expectEqual(expectedArrayQ.?.items[0], actualArrayQ.?.items[0]);
    try testing.expectEqual(expectedArrayQ.?.items[1], actualArrayQ.?.items[1]);
}

fn checkEqualRelationsLists(expected: *const ArrayList(Vector(2, u32)), actual: *const ArrayList(Vector(2, u32))) !void {
    var i: usize = 0;
    while (i < actual.*.items.len) : (i += 1) {
        try testing.expectEqual(expected.*.items[i], actual.*.items[i]);
    }
}

fn checkEqualValuationsLists(expected: *const ArrayList(ArrayList(charType)), actual: *const ArrayList(ArrayList(charType))) !void {
    var i: usize = 0;
    while (i < actual.*.items.len) : (i += 1) {
        try testing.expectEqual(expected.*.items[i].items.len, actual.*.items[i].items.len);
        try checkEqualInnerValuationsLists(&expected.*.items[i], &actual.*.items[i]);
    }
}

fn checkEqualInnerValuationsLists(expected: *const ArrayList(charType), actual: *const ArrayList(charType)) !void {
    var i: usize = 0;
    while (i < actual.*.items.len) : (i += 1) {
        try testing.expectEqual(expected.*.items[i], actual.*.items[i]);
    }
}
