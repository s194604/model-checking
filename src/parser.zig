const std = @import("std");
const typeData = @import("types.zig");
const formatter = @import("formatter.zig");
const ArrayList = std.ArrayList;
const Queue = std.atomic.Queue;
const allocator = std.heap.page_allocator;
const sentinel = @import("constants.zig").sentinel;
const charType = @import("constants.zig").charType;

pub fn parseExpressionToTree(expression: [*:sentinel]const charType, tokenContainer: *ArrayList(Queue(typeData.Token).Node), context: typeData.logicalSystem) *typeData.Node {
    var tokens = formatter.tokenizeExpression(expression, tokenContainer, context);
    return parseTokensToTree(tokens, tokenContainer.items.len, context);
}

pub fn parseTokensToTree(tokens: *Queue(typeData.Token), length: usize, context: typeData.logicalSystem) *typeData.Node {
    var currentToken: ?*Queue(typeData.Token).Node = undefined;
    var nodeContainer = ArrayList(typeData.Node).init(allocator);
    nodeContainer.ensureTotalCapacity(length) catch unreachable;
    var tree = parse(&currentToken, tokens, &nodeContainer, context);
    return tree;
}

fn parse(currentToken: *?*Queue(typeData.Token).Node, tokens: *Queue(typeData.Token), nodeContainer: *ArrayList(typeData.Node), context: typeData.logicalSystem) *typeData.Node {
    currentToken.* = tokens.*.get();
    return parseBinary(currentToken, tokens, nodeContainer, context, 0);
}

fn parseBinary(currentToken: *?*Queue(typeData.Token).Node, tokens: *Queue(typeData.Token), nodeContainer: *ArrayList(typeData.Node), context: typeData.logicalSystem, i: u32) *typeData.Node {
    var left: *typeData.Node = undefined;
    if (typeData.binaryPrecedence(i + 1, context) != null) {
        left = parseBinary(currentToken, tokens, nodeContainer, context, i + 1);
    } else {
        left = parseUnary(currentToken, tokens, nodeContainer, context);
    }
    if (currentToken.* != null) {
        const token = currentToken.*.?.*.data;
        if (token.type == typeData.binaryPrecedence(i, context)) {
            currentToken.* = tokens.*.get();
            var node = nodeContainer.*.addOneAssumeCapacity();
            var tmp: typeData.Node = typeData.Node{
                .binary = typeData.BinaryOp{
                    .type = token.type,
                    .left = left,
                    .right = parseBinary(currentToken, tokens, nodeContainer, context, 0),
                },
            };
            node.* = tmp;
            return node;
        }
    }
    return left;
}

fn parseUnary(currentToken: *?*Queue(typeData.Token).Node, tokens: *Queue(typeData.Token), nodeContainer: *ArrayList(typeData.Node), context: typeData.logicalSystem) *typeData.Node {
    const token = currentToken.*.?.*.data;
    if (token.isUnary()) {
        currentToken.* = tokens.*.get();
        var node = nodeContainer.*.addOneAssumeCapacity();
        var tmp: typeData.Node = typeData.Node{
            .unary = typeData.UnaryOp{
                .type = token.type,
                .child = parseUnary(currentToken, tokens, nodeContainer, context),
            },
        };
        node.* = tmp;
        return node;
    }
    return parseBase(currentToken, tokens, nodeContainer, context);
}

fn parseBase(currentToken: *?*Queue(typeData.Token).Node, tokens: *Queue(typeData.Token), nodeContainer: *ArrayList(typeData.Node), context: typeData.logicalSystem) *typeData.Node {
    const token = currentToken.*.?.*.data;
    if (token.type == typeData.Type.L_PAR) {
        var node = parse(currentToken, tokens, nodeContainer, context);
        currentToken.* = tokens.*.get();
        return node;
    }
    currentToken.* = tokens.*.get();
    var node = nodeContainer.*.addOneAssumeCapacity();
    if (token.type == typeData.Type.VERUM) {
        var tmp: typeData.Node = typeData.Node{ .boolean = typeData.Bool{
            .value = true,
        } };
        node.* = tmp;
        return node;
    } else if (token.type == typeData.Type.FALSUM) {
        var tmp: typeData.Node = typeData.Node{ .boolean = typeData.Bool{
            .value = false,
        } };
        node.* = tmp;
        return node;
    }
    var tmp: typeData.Node = typeData.Node{
        .proposition = typeData.Prop{
            .value = token.value,
        },
    };
    node.* = tmp;
    return node;
}
