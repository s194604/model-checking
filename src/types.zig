const std = @import("std");
const charType = @import("constants.zig").charType;
const Vector = std.meta.Vector;
const ArrayList = std.ArrayList;

pub const logicalSystem = enum {
    PROPOSITIONAL,
    MODAL,
    TEMPORAL,
};

pub const Kripke = struct {
    w: u32,
    r: *const ArrayList(Vector(2, u32)),
    v: *const ArrayList(ArrayList(charType)),
};

pub const linearValuation = struct {
    from: u32,
    until: u32,
};

pub const Type = enum {
    NOT,
    AND,
    OR,
    IF,
    IFF,
    MODAL_POSSIBLE,
    MODAL_ALWAYS,
    TEMPORAL_NEXT,
    TEMPORAL_FUTURE,
    TEMPORAL_GLOBAL,
    TEMPORAL_UNTIL,
    TEMPORAL_RELEASE,
    TEMPORAL_WEAKUNTIL,
    L_PAR,
    R_PAR,
    VERUM,
    FALSUM,
    PROPOSITION,
    pub fn isCommutative(self: Type) bool {
        if (self == Type.IF) {
            return false;
        }
        return true;
    }
};

pub const Token = struct {
    type: Type,
    value: charType,
    pub fn isUnary(self: Token) bool {
        return self.tokenType() == TokenType.UNARY;
    }
    fn tokenType(self: Token) TokenType {
        return switch (self.type) {
            Type.NOT, Type.MODAL_POSSIBLE, Type.MODAL_ALWAYS, Type.TEMPORAL_NEXT, Type.TEMPORAL_FUTURE, Type.TEMPORAL_GLOBAL => TokenType.UNARY,
            Type.AND, Type.OR, Type.IF, Type.IFF, Type.TEMPORAL_UNTIL, Type.TEMPORAL_RELEASE, Type.TEMPORAL_WEAKUNTIL => TokenType.BINARY,
            Type.L_PAR, Type.R_PAR => TokenType.PAREN,
            else => TokenType.BASE,
        };
    }
};

pub const TokenType = enum {
    UNARY,
    BINARY,
    PAREN,
    BASE,
};

pub const Node = union(enum) {
    boolean: Bool,
    proposition: Prop,
    unary: UnaryOp,
    binary: BinaryOp,
    pub fn getLabel(self: Node) ?u32 {
        switch (self) {
            .boolean => |b| return b.label,
            .proposition => |p| return p.label,
            .unary => |u| return u.label,
            .binary => |b| return b.label,
        }
    }
    pub fn printTree(self: Node, spaces: u32) void {
        printSpaces(spaces);
        switch (self) {
            .boolean => {
                std.debug.print("{}\n", .{self.boolean});
            },
            .proposition => {
                std.debug.print("{}\n", .{self.proposition});
            },
            .unary => {
                std.debug.print("{}, .label = {?}\n", .{ self.unary.type, self.unary.label });
                self.unary.child.*.printTree(spaces + 1);
            },
            .binary => {
                std.debug.print("{}, .label = {?}\n", .{ self.binary.type, self.binary.label });
                self.binary.left.*.printTree(spaces + 1);
                self.binary.right.*.printTree(spaces + 1);
            },
        }
    }
    fn printSpaces(spaces: u32) void {
        var i = spaces;
        while (i > 0) : (i -= 1) {
            std.debug.print("    ", .{});
        }
    }
};

pub const Bool = struct {
    value: bool,
    label: ?u32 = null,
};

pub const Prop = struct {
    value: charType,
    label: ?u32 = null,
};

pub const UnaryOp = struct {
    type: Type,
    child: *Node,
    label: ?u32 = null,
};

pub const BinaryOp = struct {
    type: Type,
    left: *Node,
    right: *Node,
    label: ?u32 = null,
};

pub fn characterType(char: charType, context: logicalSystem) ?Type {
    return switch (context) {
        .PROPOSITIONAL => characterTypePropositional(char),
        .MODAL => characterTypeModal(char),
        .TEMPORAL => characterTypeTemporal(char),
    };
}

pub fn characterTypePropositional(char: charType) ?Type {
    return switch (char) {
        '¬' => Type.NOT,
        '∧' => Type.AND,
        '∨' => Type.OR,
        '→' => Type.IF,
        '↔' => Type.IFF,
        '(' => Type.L_PAR,
        ')' => Type.R_PAR,
        '⊤' => Type.VERUM,
        '⊥' => Type.FALSUM,
        else => null,
    };
}

pub fn characterTypeModal(char: charType) ?Type {
    return switch (char) {
        '◇' => Type.MODAL_POSSIBLE,
        '□' => Type.MODAL_ALWAYS,
        else => characterTypePropositional(char),
    };
}

pub fn characterTypeTemporal(char: charType) ?Type {
    return switch (char) {
        'X' => Type.TEMPORAL_NEXT,
        'F' => Type.TEMPORAL_FUTURE,
        'G' => Type.TEMPORAL_GLOBAL,
        'U' => Type.TEMPORAL_UNTIL,
        'R' => Type.TEMPORAL_RELEASE,
        'W' => Type.TEMPORAL_WEAKUNTIL,
        else => characterTypePropositional(char),
    };
}

pub fn binaryPrecedence(current: u32, context: logicalSystem) ?Type {
    return switch (context) {
        logicalSystem.PROPOSITIONAL, logicalSystem.MODAL => propositionalBinaryPrecedence(current),
        logicalSystem.TEMPORAL => temporalBinaryPrecedence(current),
    };
}

pub fn propositionalBinaryPrecedence(current: u32) ?Type {
    return switch (current) {
        0 => Type.IFF,
        1 => Type.IF,
        2 => Type.OR,
        3 => Type.AND,
        else => null,
    };
}

pub fn temporalBinaryPrecedence(current: u32) ?Type {
    return switch (current) {
        0 => Type.TEMPORAL_WEAKUNTIL,
        1 => Type.TEMPORAL_UNTIL,
        2 => Type.TEMPORAL_RELEASE,
        3 => Type.IFF,
        4 => Type.IF,
        5 => Type.OR,
        6 => Type.AND,
        else => null,
    };
}
