/** @type {import('./$types').PageLoad} */

export function load({params}) {
    return {
        checkTemp: checkTemporal
    };
}

function formatTemporalOutput(outputA, outputB, length) {
    if (length == 0) {
        return "No valid nodes.";
    }
    var outputString = "Valid nodes: ";
    var i = 0;
    var a;
    var b;
    while (i < length) {
        if (outputA[i] == 0) {a = '∞'} else {a = outputA[i]}
        if (outputB[i] == 0) {b = '∞'} else {b = outputB[i]}
        if (a == b) {outputString += a} else {
            outputString += a;
            outputString += "..";
            outputString += b;
        }
        if (i + 1 == length) {
            outputString += ". ";
        } else {
            outputString += ", ";
        }
        i += 1;
    }
    return outputString;
}

async function checkTemporal(v, e) {
    const instance = await WebAssembly.instantiate(await WebAssembly.compileStreaming(fetch("./checker.wasm?url")), {
        printer: {
            print(x) {
              console.log(x);
            }
        },
        nodePusher: {
            pushNode(label, length, isValid) {}
        },
        edgePusher: {
            pushEdge(from, to) {}
        },
    });

    const arr0 = Array.from(v);
    const arr1 = Array.from(e);

    const input0 = new Uint32Array(instance.exports.memory.buffer, 0, arr0.length + 1);
    for (var i = arr0.length - 1; i >= 0; i--) {
        input0[i] = arr0[i].charCodeAt(0);
    }
    input0[arr0.length] = 0;
    const input1 = new Uint32Array(instance.exports.memory.buffer, input0.byteOffset + (arr0.length + 1) * 4, arr1.length + 1);
    for (var i = arr1.length - 1; i >= 0; i--) {
        input1[i] = arr1[i].charCodeAt(0);
    }
    input1[arr1.length] = 0;
    var output = new Uint32Array(instance.exports.memory.buffer);
    const length = instance.exports.checkTemporal(input0.byteOffset, input1.byteOffset, output);
    output = new Uint32Array(instance.exports.memory.buffer, output.byteOffset, 2);
    const outputA = new Uint32Array(instance.exports.memory.buffer, output[0], length);
    const outputB = new Uint32Array(instance.exports.memory.buffer, output[1], length);
    return formatTemporalOutput(outputA, outputB, length);
}