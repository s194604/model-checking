/** @type {import('./$types').PageLoad} */

export function load({params}) {
    return {
        checkModal: checkModal
    };
}

function formatPropOutput(outputArray) {
    if (outputArray[0] == 0 || outputArray[0] == undefined) {
        return "No valid nodes.";
    }
    var outputString = "Valid nodes: ";
    var i = 0;
    while (outputArray[i] != 0 && outputArray[i] != undefined) {
        outputString += outputArray[i];
        if (outputArray[i + 1] == 0 || outputArray[i + 1] == undefined) {
            outputString += ". ";
        } else {
            outputString += ", ";
        }
        i += 1;
    }
    return outputString;
}


function formatModalOutput(outputArray) {
    return formatPropOutput(outputArray);
}

async function checkModal(w, r, v, e) {
    const instance = await WebAssembly.instantiate(await WebAssembly.compileStreaming(fetch("./checker.wasm?url")), {
        printer: {
            print(x) {
              console.log(x);
            }
        },
        nodePusher: {
            pushNode(label, length, isValid) {}
        },
        edgePusher: {
            pushEdge(from, to) {}
        },
    });

    const arr0 = Array.from(r);
    const arr1 = Array.from(v);
    const arr2 = Array.from(e);

    const input0 = new Uint32Array(instance.exports.memory.buffer, 0, arr0.length + 1);
    for (var i = arr0.length - 1; i >= 0; i--) {
        input0[i] = arr0[i].charCodeAt(0);
    }
    input0[arr0.length] = 0;
    const input1 = new Uint32Array(instance.exports.memory.buffer, input0.byteOffset + (arr0.length + 1) * 4, arr1.length + 1);
    for (var i = arr1.length - 1; i >= 0; i--) {
        input1[i] = arr1[i].charCodeAt(0);
    }
    input1[arr1.length] = 0;
    const input2 = new Uint32Array(instance.exports.memory.buffer, input1.byteOffset + (arr1.length + 1) * 4, arr2.length + 1);
    for (var i = arr2.length - 1; i >= 0; i--) {
        input2[i] = arr2[i].charCodeAt(0);
    }
    input2[arr2.length] = 0;
    var output = new Uint32Array(instance.exports.memory.buffer);
    instance.exports.checkModal(w, input0.byteOffset, input1.byteOffset, input2.byteOffset, output);
    output = new Uint32Array(instance.exports.memory.buffer, output.byteOffset, w);
    return formatModalOutput(output);
}